// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "hglib.h"

#include <string>
#include <utility>
#include <vector>

#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using hglib::DirectedVertex;
using hglib::UndirectedVertex;
using hglib::DirectedHyperedge;
using hglib::UndirectedHyperedge;
using hglib::NamedDirectedHyperedge;
using hglib::NamedUndirectedHyperedge;

class TestGraphConversion : public testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<vector<string>, string>> hyperedges {
      {{"A", "Foo"}, "edge1"},
      {{"A", "B", "Bar", ""}, "edge2"},
      {{"6", ""}, "edge3"}
  };
  vector<pair<pair<vector<string>, vector<string>>, string>> hyperarcs {
    {{{"A"}, {"Foo"}}, "edge1"},
    {{{"A", "B"}, {"Bar", ""}}, "edge2"},
    {{{"6"}, {""}}, "edge3"}
  };
};

// Create ad-hoc properties
enum class Colour {red, blue, green};
struct VertexProperty {
  Colour colour = Colour::green;
};
struct EdgeProperty {
  double weight = 0.0;
};
struct GraphProperty {
  std::string label;
};

TEST_F(TestGraphConversion, Test_DhgConversion) {
  // Create a graph with no properties
  hglib::DirectedHypergraph <DirectedVertex,
                             DirectedHyperedge> g1;
  for (auto vertexName : vertices) {
    g1.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g1.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // Copy graph into graph with properties
  hglib::DirectedHypergraph <DirectedVertex,
                             DirectedHyperedge,
                             VertexProperty,
                             EdgeProperty,
                             GraphProperty> g2(g1);

  // Copy back into graph with empty properties
  hglib::DirectedHypergraph <DirectedVertex,
                             DirectedHyperedge,
                             hglib::emptyProperty,
                             hglib::emptyProperty,
                             hglib::emptyProperty> g3(g2);

  // Check nb of vertices and hyperarcs
  ASSERT_EQ(g1.nbVertices(), g2.nbVertices());
  ASSERT_EQ(g1.nbVertices(), g3.nbVertices());
  ASSERT_EQ(g1.nbHyperarcs(), g2.nbHyperarcs());
  ASSERT_EQ(g1.nbHyperarcs(), g3.nbHyperarcs());
  // Check vertices
  auto vertexIt = g1.verticesBegin();
  auto vertexEnd = g1.verticesEnd();
  for (; vertexIt != vertexEnd; ++vertexIt) {
    const auto& g1Vertex = *vertexIt;
    const auto& g2Vertex = g2.vertexByName(g1Vertex->name());
    const auto& g3Vertex = g3.vertexByName(g1Vertex->name());
    ASSERT_NE(g2Vertex, nullptr);
    ASSERT_NE(g3Vertex, nullptr);
    ASSERT_EQ(g1Vertex->id(), g2Vertex->id());
    ASSERT_EQ(g1Vertex->id(), g3Vertex->id());
    const auto& vertexId = g1Vertex->id();
    // compare in-hyperarcs
    ASSERT_EQ(g1.inDegree(vertexId), g2.inDegree(vertexId));
    ASSERT_EQ(g1.inDegree(vertexId), g3.inDegree(vertexId));
    auto hyperarcIt = g1.inHyperarcsBegin(vertexId);
    auto hyperarcEnd = g1.inHyperarcsEnd(vertexId);
    auto g2HyperarcIt = g2.inHyperarcsBegin(vertexId);
    auto g3HyperarcIt = g3.inHyperarcsBegin(vertexId);
    for (; hyperarcIt != hyperarcEnd; ++hyperarcIt) {
      const auto& hyperarc = *hyperarcIt;
      ASSERT_EQ(hyperarc->id(), (*g2HyperarcIt)->id());
      ASSERT_EQ(hyperarc->id(), (*g3HyperarcIt)->id());
      ++g2HyperarcIt;
      ++g3HyperarcIt;
    }
    // compare out-edges
    ASSERT_EQ(g1.outDegree(vertexId), g2.outDegree(vertexId));
    ASSERT_EQ(g1.outDegree(vertexId), g3.outDegree(vertexId));
    hyperarcIt = g1.outHyperarcsBegin(vertexId);
    hyperarcEnd = g1.outHyperarcsEnd(vertexId);
    g2HyperarcIt = g2.outHyperarcsBegin(vertexId);
    g3HyperarcIt = g3.outHyperarcsBegin(vertexId);
    for (; hyperarcIt != hyperarcEnd; ++hyperarcIt) {
      const auto& hyperarc = *hyperarcIt;
      ASSERT_EQ(hyperarc->id(), (*g2HyperarcIt)->id());
      ASSERT_EQ(hyperarc->id(), (*g3HyperarcIt)->id());
      ++g2HyperarcIt;
      ++g3HyperarcIt;
    }
  }

  // Check hyperarcs
  auto itHyperarcs = g1.hyperarcsBegin();
  auto itHyperarcsEnd = g1.hyperarcsEnd();
  for (; itHyperarcs != itHyperarcsEnd; ++itHyperarcs) {
    const auto& g1Hyperarc = *itHyperarcs;
    const auto& g2Hyperarc = g2.hyperarcById(g1Hyperarc->id());
    const auto& g3Hyperarc = g3.hyperarcById(g1Hyperarc->id());
    ASSERT_NE(g2Hyperarc, nullptr);
    ASSERT_NE(g3Hyperarc, nullptr);
    ASSERT_EQ(g1Hyperarc->id(), g2Hyperarc->id());
    ASSERT_EQ(g1Hyperarc->id(), g3Hyperarc->id());
    const auto& g1HyperarcId = g1Hyperarc->id();
    // compare tails
    ASSERT_EQ(g1.nbTailVertices(g1HyperarcId), g2.nbTailVertices(g1HyperarcId));
    ASSERT_EQ(g1.nbTailVertices(g1HyperarcId), g3.nbTailVertices(g1HyperarcId));
    auto g2vertexIt = g2.tailsBegin(g1HyperarcId);
    auto g3vertexIt = g3.tailsBegin(g1HyperarcId);
    auto it = g1.tailsBegin(g1HyperarcId);
    auto end = g1.tailsEnd(g1HyperarcId);
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_EQ(tail->id(), (*g2vertexIt)->id());
      ASSERT_EQ(tail->id(), (*g3vertexIt)->id());
      ASSERT_EQ(tail->name().compare((*g2vertexIt)->name()), 0);
      ASSERT_EQ(tail->name().compare((*g3vertexIt)->name()), 0);
      ++g2vertexIt;
      ++g3vertexIt;
    }
    // compare heads
    ASSERT_EQ(g1.nbHeadVertices(g1HyperarcId), g2.nbHeadVertices(g1HyperarcId));
    ASSERT_EQ(g1.nbHeadVertices(g1HyperarcId), g3.nbHeadVertices(g1HyperarcId));
    g2vertexIt = g2.headsBegin(g1HyperarcId);
    g3vertexIt = g3.headsBegin(g1HyperarcId);

    it = g1.headsBegin(g1HyperarcId);
    end = g1.headsEnd(g1HyperarcId);
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_EQ(head->id(), (*g2vertexIt)->id());
      ASSERT_EQ(head->id(), (*g3vertexIt)->id());
      ASSERT_EQ(head->name().compare((*g2vertexIt)->name()), 0);
      ASSERT_EQ(head->name().compare((*g3vertexIt)->name()), 0);
      ++g2vertexIt;
      ++g3vertexIt;
    }
  }

  // Add a vertex and hyperarc to converted graphs to check their correct id
  const auto& g2AddedVertex = g2.addVertex("Test");
  const auto& g3AddedVertex = g3.addVertex("Test");
  const auto& g2AddedHyperarc = g2.addHyperarc(hglib::NAME, {{"A"}, {"Test"}});
  const auto& g3AddedHyperarc = g3.addHyperarc(hglib::NAME, {{"A"}, {"Test"}});
  ASSERT_EQ(g2AddedVertex->id(), 6);
  ASSERT_EQ(g3AddedVertex->id(), 6);
  ASSERT_EQ(g2AddedHyperarc->id(), 3);
  ASSERT_EQ(g3AddedHyperarc->id(), 3);
}

TEST_F(TestGraphConversion, Test_UhgConversion) {
  // Create a graph with no properties
  hglib::UndirectedHypergraph<UndirectedVertex,
                              UndirectedHyperedge> g1;
  for (auto vertexName : vertices) {
    g1.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g1.addHyperedge(hglib::NAME, hyperedge.first);
  }

  // Copy graph into graph with properties
  hglib::UndirectedHypergraph<UndirectedVertex,
                              UndirectedHyperedge,
                              VertexProperty,
                              EdgeProperty,
                              GraphProperty> g2(g1);

  // Copy back into graph with empty properties
  hglib::UndirectedHypergraph<UndirectedVertex,
                              UndirectedHyperedge,
                              hglib::emptyProperty,
                              hglib::emptyProperty,
                              hglib::emptyProperty> g3(g2);
  // Check nb of vertices and hyperarcs
  ASSERT_EQ(g1.nbVertices(), g2.nbVertices());
  ASSERT_EQ(g1.nbVertices(), g3.nbVertices());
  ASSERT_EQ(g1.nbHyperedges(), g2.nbHyperedges());
  ASSERT_EQ(g1.nbHyperedges(), g3.nbHyperedges());
  // Check vertices
  for (const auto& vertex : g1.vertices()) {
    const auto& g2Vertex = g2.vertexByName(vertex->name());
    const auto& g3Vertex = g3.vertexByName(vertex->name());
    ASSERT_NE(g2Vertex, nullptr);
    ASSERT_NE(g3Vertex, nullptr);
    ASSERT_EQ(vertex->id(), g2Vertex->id());
    ASSERT_EQ(vertex->id(), g3Vertex->id());
    const auto& vertexId = vertex->id();
    // Compare implied hyperedges
    ASSERT_EQ(g1.nbHyperedges(vertexId), g2.nbHyperedges(vertexId));
    ASSERT_EQ(g1.nbHyperedges(vertexId), g3.nbHyperedges(vertexId));
    auto hyperedgeIt = g1.hyperedgesBegin(vertexId);
    auto hyperedgeEnd = g1.hyperedgesEnd(vertexId);
    auto g2HyperedgeIt = g2.hyperedgesBegin(vertexId);
    auto g3HyperedgeIt = g3.hyperedgesBegin(vertexId);
    for (; hyperedgeIt != hyperedgeEnd; ++hyperedgeIt) {
      const auto& hyperedge = *hyperedgeIt;
      ASSERT_EQ(hyperedge->id(), (*g2HyperedgeIt)->id());
      ASSERT_EQ(hyperedge->id(), (*g3HyperedgeIt)->id());
      ++g2HyperedgeIt;
      ++g3HyperedgeIt;
    }
  }

  // Check hyperedges
  for (const auto& hyperedge : g1.hyperedges()) {
    const auto& g2Hyperarc = g2.hyperedgeById(hyperedge->id());
    const auto& g3Hyperarc = g3.hyperedgeById(hyperedge->id());
    ASSERT_NE(g2Hyperarc, nullptr);
    ASSERT_NE(g3Hyperarc, nullptr);
    ASSERT_EQ(hyperedge->id(), g2Hyperarc->id());
    ASSERT_EQ(hyperedge->id(), g3Hyperarc->id());
    const auto& hyperedgeId = hyperedge->id();
    // Compare impliedVertices
    ASSERT_EQ(g1.nbImpliedVertices(hyperedgeId),
              g2.nbImpliedVertices(hyperedgeId));
    ASSERT_EQ(g1.nbImpliedVertices(hyperedgeId),
              g3.nbImpliedVertices(hyperedgeId));
    auto impliedVerticesIt = g1.impliedVerticesBegin(hyperedgeId);
    auto impliedVerticesEnd = g1.impliedVerticesEnd(hyperedgeId);
    auto g2VertexIt = g2.impliedVerticesBegin(hyperedgeId);
    auto g3VertexIt = g3.impliedVerticesBegin(hyperedgeId);
    for (; impliedVerticesIt != impliedVerticesEnd; ++impliedVerticesIt) {
      const auto& v = *impliedVerticesIt;
      ASSERT_EQ(v->id(), (*g2VertexIt)->id());
      ASSERT_EQ(v->id(), (*g3VertexIt)->id());
      ASSERT_EQ(v->name().compare((*g2VertexIt)->name()), 0);
      ASSERT_EQ(v->name().compare((*g3VertexIt)->name()), 0);
      ++g2VertexIt;
      ++g3VertexIt;
    }
  }

  // Add a vertex and hyperedge to converted graphs to check their correct id
  const auto& g2AddedVertex = g2.addVertex("Test");
  const auto& g3AddedVertex = g3.addVertex("Test");
  const auto& g2AddedHyperedge = g2.addHyperedge(hglib::NAME,
                                                 {"A", "Test"});
  const auto& g3AddedHyperedge = g3.addHyperedge(hglib::NAME,
                                                 {"A", "Test"});
  ASSERT_EQ(g2AddedVertex->id(), 6);
  ASSERT_EQ(g3AddedVertex->id(), 6);
  ASSERT_EQ(g3AddedHyperedge->id(), 3);
  ASSERT_EQ(g2AddedHyperedge->id(), 3);
}