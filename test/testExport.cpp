// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "hglib.h"

#include <iostream>
#include <string>
#include <vector>


#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using hglib::DirectedVertex;
using hglib::DirectedHyperedge;
using hglib::UndirectedHyperedge;
using hglib::NamedDirectedHyperedge;

/**
 * Test fixture
 */
class TestExport : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<pair<vector<string>, vector<string>>, string>> hyperarcs {
    {{{"A"}, {"Foo"}}, "edge1"},
    {{{"A", "B"}, {"Bar", ""}}, "edge2"},
    {{{"6"}, {""}}, "edge3"}
  };
  vector<pair<vector<string>, string>> hyperedges {
    {{"A", "Foo"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"}
  };

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestExport, export_directedHypergraph) {
  hglib::DirectedHypergraph<> directedHypergraph;

  // insert vertices
  for (const auto& vertexName : vertices) {
    directedHypergraph.addVertex(vertexName);
  }
  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    directedHypergraph.addHyperarc(hglib::NAME, hyperarc.first);
  }

  hglib::exportHypergraph(directedHypergraph, "dirHypergraph.out");

  // Read the file in and check the content
  std::ifstream in("dirHypergraph.out");
  std::stringstream observedContent;
  observedContent << in.rdbuf();
  std::string expectedContent = "# Isolated vertices\n"
      "# Hyperarcs\n"
      "{\"A\"} -> {\"Foo\"}\n"
      "{\"A\", \"B\"} -> {\"Bar\", \"\"}\n"
      "{\"6\"} -> {\"\"}\n";
  ASSERT_EQ(expectedContent, observedContent.str());
}

TEST_F(TestExport, export_directedHypergraph_with_isolatedVertices) {
  hglib::DirectedHypergraph<> directedHypergraph;

  // insert vertices
  for (const auto& vertexName : vertices) {
    directedHypergraph.addVertex(vertexName);
  }
  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    directedHypergraph.addHyperarc(hglib::NAME, hyperarc.first);
  }

  directedHypergraph.removeHyperarc(1);
  hglib::exportHypergraph(directedHypergraph, "dirHypergraph_isolated.out");

  // Read the file in and check the content
  std::ifstream in("dirHypergraph_isolated.out");
  std::stringstream observedContent;
  observedContent << in.rdbuf();
  std::string expectedContent = "# Isolated vertices\n"
      "\"B\", \"Bar\"\n"
      "# Hyperarcs\n"
      "{\"A\"} -> {\"Foo\"}\n"
      "{\"6\"} -> {\"\"}\n";
  ASSERT_EQ(expectedContent, observedContent.str());
}

TEST_F(TestExport, export_undirectedHypergraph) {
  hglib::UndirectedHypergraph<> undirectedHypergraph;
  // insert vertices
  for (const auto& vertexName : vertices) {
    undirectedHypergraph.addVertex(vertexName);
  }
  // add hyperarcs
  for (auto hyperedge : hyperedges) {
    undirectedHypergraph.addHyperedge(hglib::NAME, hyperedge.first);
  }

  hglib::exportHypergraph(undirectedHypergraph, "undirHypergraph.out");

  // Read the file in and check the content
  std::ifstream in("undirHypergraph.out");
  std::stringstream observedContent;
  observedContent << in.rdbuf();
  std::string expectedContent = "# Isolated vertices\n"
      "# Hyperedges\n"
      "{\"A\", \"Foo\"}\n"
      "{\"A\", \"B\", \"Bar\", \"\"}\n"
      "{\"6\", \"\"}\n";
  ASSERT_EQ(expectedContent, observedContent.str());
}

TEST_F(TestExport, export_undirectedHypergraph_with_isolatedVertices) {
  hglib::UndirectedHypergraph<> undirectedHypergraph;
  // insert vertices
  for (const auto& vertexName : vertices) {
    undirectedHypergraph.addVertex(vertexName);
  }
  // add hyperarcs
  for (auto hyperedge : hyperedges) {
    undirectedHypergraph.addHyperedge(hglib::NAME, hyperedge.first);
  }

  undirectedHypergraph.removeHyperedge(1);
  hglib::exportHypergraph(undirectedHypergraph, "undirHypergraph_isolated.out");

  // Read the file in and check the content
  std::ifstream in("undirHypergraph_isolated.out");
  std::stringstream observedContent;
  observedContent << in.rdbuf();
  std::string expectedContent = "# Isolated vertices\n"
      "\"B\", \"Bar\"\n"
      "# Hyperedges\n"
      "{\"A\", \"Foo\"}\n"
      "{\"6\", \"\"}\n";
  ASSERT_EQ(expectedContent, observedContent.str());
}
