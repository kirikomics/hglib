// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include <hglib/hyperedge/undirected/UndirectedHyperedge.h>
#include "hglib/hyperedge/undirected/NamedUndirectedHyperedge.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/hyperedge/directed/NamedDirectedHyperedge.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

#include "gtest/gtest.h"

class TestIsParentOf : public testing::Test {
 protected:
  virtual void SetUp() {}

  virtual void TearDown() {}

  template <typename T>
  struct A {};
  struct B : A<int> {};
  struct C : A<float> {};
};

TEST_F(TestIsParentOf, Test_struct) {
  bool returnType = hglib::is_parent_of<A, B>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<A, C>::value;
  ASSERT_TRUE(returnType);

  // Test hglib hyperedge/hyperarc hierarchy
  returnType = hglib::is_parent_of<hglib::DirectedHyperedgeBase,
          hglib::DirectedHyperedge>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedHyperedgeBase,
          hglib::NamedDirectedHyperedge>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedHyperedgeBase,
          hglib::DirectedHyperedge>::value;
  ASSERT_FALSE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedHyperedgeBase,
          hglib::NamedDirectedHyperedge>::value;
  ASSERT_FALSE(returnType);

  returnType = hglib::is_parent_of<hglib::UndirectedHyperedgeBase,
          hglib::UndirectedHyperedge>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedHyperedgeBase,
          hglib::NamedUndirectedHyperedge>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedHyperedgeBase,
          hglib::UndirectedHyperedge>::value;
  ASSERT_FALSE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedHyperedgeBase,
          hglib::NamedUndirectedHyperedge>::value;
  ASSERT_FALSE(returnType);

  // Test hglib vertex hierarchy
  returnType = hglib::is_parent_of<hglib::DirectedVertex,
          hglib::DirectedVertex<hglib::DirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedVertex,
          hglib::DirectedVertex<hglib::NamedDirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedVertex,
          hglib::UndirectedVertex<hglib::UndirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedVertex,
          hglib::UndirectedVertex<hglib::NamedUndirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);

  // The following combinations of vertex and hyperedge type won't be accepted
  // in a (un-)directed hypergraph. But they assert true because they are valid
  // pairs of parent and derived class.
  returnType = hglib::is_parent_of<hglib::UndirectedVertex,
          hglib::UndirectedVertex<hglib::DirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::UndirectedVertex,
          hglib::UndirectedVertex<hglib::NamedDirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedVertex,
          hglib::DirectedVertex<hglib::UndirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);
  returnType = hglib::is_parent_of<hglib::DirectedVertex,
          hglib::DirectedVertex<hglib::NamedUndirectedHyperedge>>::value;
  ASSERT_TRUE(returnType);

  returnType = hglib::is_parent_of<hglib::UndirectedVertex,
          hglib::DirectedVertex<hglib::DirectedHyperedge>>::value;
  ASSERT_FALSE(returnType);
}
