#
# - Try to find gcov
#
# Once done this will define
#
#   GCOV_FOUND - system has gcov
#   GCOV_PROGRAM, the gcov executable.
#
find_program(GCOV_PROGRAM
             NAMES gcov
             PATHS /usr/bin
                   /usr/local/bin
                   ${HOME}/.local/bin
                   ${HOME}/Library/Python/2.7/bin)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gcov DEFAULT_MSG GCOV_PROGRAM)