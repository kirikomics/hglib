// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_ARGUMENTSTOCREATEVERTEX_H
#define HGLIB_ARGUMENTSTOCREATEVERTEX_H

#include <string>

namespace hglib {
/**
 * \brief ArgumentsToCreateVertex class
 *
 * \details
 * A wrapper class that holds the arguments needed to create a vertex of the
 * template parameter type. This includes the name, and attributes that are
 * specific to the vertex type.
 *
 * \tparam Vertex_t Type of vertex.
 */
template<typename Vertex_t>
class ArgumentsToCreateVertex {
  friend Vertex_t;

 protected:
  /// ArgumentsToCreateVertex constructor
  ArgumentsToCreateVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& specificAttributes);

 public:
  /// Get the name of the vertex
  const std::string& name() const;
  /// Get specific attributes
  const typename Vertex_t::SpecificAttributes& specificAttributes() const;
  /// Set name of the vertex
  void setName(const std::string& name);
  /// Set attributes that are specific to the vertex type (Vertex_t)
  void setSpecificArguments(const typename Vertex_t::SpecificAttributes&);

 protected:
  /// Name of the vertex
  std::string name_;
  /// Specific attributes for the vertex type
  typename Vertex_t::SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "ArgumentsToCreateVertex.hpp"
#endif  // HGLIB_ARGUMENTSTOCREATEVERTEX_H
