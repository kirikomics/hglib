// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "DirectedVertex.h"

#include <iostream>
#include <memory>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief DirectedVertex constructor
 *
 * \param id Identifier of the vertex.
 * \param name Name of the vertex.
 * \param specificAttributes Specific arguments of DirectedVertex.
 */
template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(
    VertexIdType id,
    std::string name,
    const SpecificAttributes& specificAttributes) :
    VertexBase(id, name) {
  inHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  outHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  specificAttributes_ =
      std::make_unique<SpecificAttributes>(specificAttributes);
}


/**
 * \brief DirectedVertex constructor
 *
 * \details
 * This constructor creates a vertex with the name 'dv_' appended by the given
 * identifier.
 *
 * \param id Identifier of the vertex.
 * \param specificAttributes Specific arguments of DirectedVertex.
 */
template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(
        VertexIdType id,
        const SpecificAttributes& specificAttributes) :
        DirectedVertex(id, "dv_" + std::to_string(id), specificAttributes) {}


/**
 * \brief DirectedVertex copy constructor
 *
 * \param vertex The to be copied DirectedVertex.
 */
template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(const DirectedVertex& other) :
        VertexBase(other) {
  inHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  outHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  specificAttributes_ =
      std::make_unique<SpecificAttributes>(*(other.specificAttributes_));
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 * ***************************************************************************/
/**
 * \brief Add an outgoing directed hyperedge
 *
 * \param hyperarc Pointer to a directed hyperedge.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::addOutHyperarc(Hyperarc_t* hyperarc) {
  outHyperarcs_->push_back(hyperarc);
}


/**
 * \brief Add an incoming directed hyperedge
 *
 * \param hyperarc Pointer to a directed hyperedge.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::addInHyperarc(Hyperarc_t* hyperarc) {
  inHyperarcs_->push_back(hyperarc);
}


/**
 * \brief Remove an outgoing directed hyperedge
 *
 * \param hyperarcToBeDeleted Const reference to a directed hyperedge.
 *
 * \par Complexity
 * Linear
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::
removeOutHyperarc(const Hyperarc_t& hyperarcToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperarc
  // in the outgoing hyperarcs.
  auto newEnd = std::remove(outHyperarcs_->data(),
                            outHyperarcs_->data() + outHyperarcs_->size(),
                            &hyperarcToBeDeleted);
  // new end is different from the old one
  if (newEnd != (outHyperarcs_->data() + outHyperarcs_->size())) {
    outHyperarcs_->resize(outHyperarcs_->size() - 1);
  }
}


/**
 * \brief Remove an incoming directed hyperedge
 *
 * \param hyperarcToBeDeleted Const reference to a directed hyperedge.
 *
 * \par Complexity
 * Linear
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::
removeInHyperarc(const Hyperarc_t& hyperarcToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperarc
  // in the incoming hyperarcs.
  auto newEnd = std::remove(inHyperarcs_->data(),
                            inHyperarcs_->data() + inHyperarcs_->size(),
                            &hyperarcToBeDeleted);
  // new end is different from
  if (newEnd != (inHyperarcs_->data() + inHyperarcs_->size())) {
    inHyperarcs_->resize(inHyperarcs_->size() - 1);
  }
}


/**
 * \brief Get arguments needed to create this vertex
 *
 * \details
 * This function returns an instance of ArgumentsToCreateVertex that
 * provides all arguments (name, specific attributes) needed to add this
 * directed vertex to a hypergraph.
 *
 * \return ArgumentsToCreateVertex<DirectedVertex<Hyperarc_t>> Arguments to
 * add a directed vertex to a hypergraph via the function addVertex().
 */
template <typename Hyperarc_t>
ArgumentsToCreateVertex<DirectedVertex<Hyperarc_t>>
DirectedVertex<Hyperarc_t>::
argumentsToCreateVertex() const {
  return ArgumentsToCreateVertex<DirectedVertex<Hyperarc_t>>(
          name_, *specificAttributes_);
}


/**
 * \brief Compare function
 *
 * \details
 * Compare the given name with the vertex name. The second parameter is ignored
 * in the DirectedVertex class because the nested class SpecificAttributes is
 * empty and thus there is nothing to compare. However, other classes modeling
 * directed vertices may contain attributes in this nested class. The function
 * is called independent of the vertex type in the Hypergraph as
 * vertex.compare(someName, someSpecArgs);
 *
 * \param name String to compare with the vertex name.
 * \param SpecificAttributes Specific arguments of DirectedVertex.
 *
 * \return true if the given name is identical to this vertex name, false
 * otherwise.
 */
template <typename Hyperarc_t>
bool DirectedVertex<Hyperarc_t>::
compare(const std::string& name, const SpecificAttributes&) const {
  return (name.compare(name_) == 0);
}
}  // namespace hglib
