// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDVERTEX_H
#define HGLIB_UNDIRECTEDVERTEX_H

#include "hglib/vertex/VertexBase.h"

#include <memory>
#include <vector>

#include "hglib/vertex/ArgumentsToCreateVertex.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedgeBase.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"

namespace hglib {

template <typename UndirectedVertex_t>
class UndirectedHyperedgeBase;

/**
 * \brief Basic class of an undirected vertex
 *
 * \details
 * This is a base class of an undirected vertex. This class stores in a
 * container all undirected hyperedges that contain the vertex. Furthermore,
 * attributes that are specific to a type of undirected vertex are stored in an
 * object of the nested class SpecificAttributes (class without attributes in
 * the class UndirectedVertex). This architecture allows to add any kind of
 * undirected vertex to an undirected hypergraph via the function addVertex().
 * One can use a custom class of an undirected vertex in an undirected
 * hypergraph under the condition that this class is derived from
 * UndirectedVertex and that it contains a nested class SpecificAttributes.
 *
 * \tparam Hyperedge_t Type that is used to store undirected hyperedges (of
 * type Hyperedge_t).
 */
template <typename Hyperedge_t>
class UndirectedVertex : public VertexBase {
  template <template <typename> typename Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;
  template <template <typename> typename Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraph;
  template <template <typename> typename Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

 public:
  // Assert that Hyperedge_t is derived from UndirectedHyperedgeBase
  static_assert(is_parent_of<UndirectedHyperedgeBase, Hyperedge_t>::value,
                "Template parameter of UndirectedVertex must be a class "
                        "derived from UndirectedHyperedgeBase");
  /// Alias to make the template parameter publicly available
  using HyperedgeType = Hyperedge_t;

 protected:
  /// UndirectedVertex constructor
  UndirectedVertex(VertexIdType id, std::string name,
                   const SpecificAttributes& specificAttributes);
  /// UndirectedVertex constructor
  UndirectedVertex(VertexIdType id,
                   const SpecificAttributes& specificAttributes);
  /// UndirectedVertex copy constructor
  UndirectedVertex(const UndirectedVertex& other);
  /// UndirectedVertex destructor
  ~UndirectedVertex() {}
  /// Add an undirected hyperedge
  void addHyperedge(Hyperedge_t* hyperedge);
  /// Remove an undirected hyperedge
  void removeHyperedge(const Hyperedge_t& hyperedgeToBeDeleted);
  /// Get arguments needed to create this UndirectedVertex
  ArgumentsToCreateVertex<
          UndirectedVertex<Hyperedge_t>> argumentsToCreateVertex() const;
  /// Compare function
  bool compare(const std::string& name, const SpecificAttributes& args) const;

 protected:
  /// Hyperedges that contain the undirected vertex
  std::shared_ptr<GraphElementContainer<Hyperedge_t*>> hyperedges_;
};
}  // namespace hglib

#include "UndirectedVertex.hpp"
#endif  // HGLIB_UNDIRECTEDVERTEX_H
