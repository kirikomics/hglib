// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_VERTEXBASE_H
#define HGLIB_VERTEXBASE_H

#include <string>

#include "hglib/utils/types.h"
#include "ArgumentsToCreateVertex.h"

namespace hglib {
/**
 * \brief Vertex base class
 *
 * \details
 * This base class contains attributes that are common to all types of vertices,
 * that is an identifier and a name.
 */
class VertexBase {
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;

 public:
  /**
   * \brief SpecificAttributes nested class
   *
   * \details
   * The nested class SpecificAttributes allows classes derived from
   * VertexBase to specify additional attributes in a packed and
   * unified way.
   */
  class SpecificAttributes {};

 protected:
  /// VertexBase constructor
  inline VertexBase(VertexIdType id, std::string name);
  /// VertexBase constructor
  inline VertexBase(VertexIdType id);
  /// VertexBase copy constructor
  inline VertexBase(const VertexBase& other);
  /// VertexBase destructor
  inline virtual ~VertexBase() = default;

 public:
  /// Get the identifier of the vertex
  inline VertexIdType id() const;
  /// Get the name of the vertex
  inline std::string name() const;
  /// Print the vertex
  inline virtual void print(std::ostream& out) const;

 protected:
  /// Assign an identifier to the vertex
  inline void setId(const VertexIdType& id);

 protected:
  /// Identifier of the vertex
  VertexIdType id_;
  /// Name of the vertex
  std::string name_;
  /// Specific attributes
  std::unique_ptr<SpecificAttributes> specificAttributes_;
};

}  // namespace hglib

#include "hglib/vertex/VertexBase.hpp"

#endif  // HGLIB_VERTEXBASE_H
