// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_TYPES_H
#define HGLIB_TYPES_H

#include <sys/types.h>

#include "filtered_vector.h"

namespace hglib {
  /**
   * \brief Alias for container wherein vertices, hyperedges are stored
   *
   * \ingroup hglibScope
   */
  template<typename T>
  using GraphElementContainer = FilteredVector<T>;

  /**
   * \brief Alias for the vertex Id type
   *
   * \ingroup hglibScope
   */
  using VertexIdType = u_int16_t;

  /**
   * \brief Alias for the hyperedge Id type
   *
   * \ingroup hglibScope
   */
  using HyperedgeIdType = u_int16_t;

  /**
   * \brief Empty default property struct.
   *
   * \details
   * This struct is used as default template parameter for a vertex, hyperedge
   * or hypergraph property.
   *
   * \ingroup hglibScope
   */
  struct emptyProperty{};

  /**
   * \brief Flag, used to distinguish if the vertices are given by id or by name
   *
   * \details
   * Flag, used to distinguish if the vertices are given by id or by name
   * in the member function addHyperarc() in a DirectedHypergraph, or
   * addHyperedge() in a UndirectedHypergraph.
   *
   * \ingroup hglibScope
   */
  constexpr u_int8_t NAME = 0;

}  // namespace hglib
#endif  // HGLIB_TYPES_H
