// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_OBSERVABLE_H
#define HGLIB_OBSERVABLE_H

#include "ObservableEvent.h"
#include "Observer.h"

#include <list>
#include <map>

namespace hglib {

/**
 * \brief Observable class
 *
 * \details
 * This class takes the role of the subject (that is observed) in the observer
 * pattern.
 */
class Observable {
 public :
  // ==========================================================================
  //                               Constructors
  // ==========================================================================
  /// Default ctor
  Observable() = default;
  /// Copy ctor
  Observable(const Observable&) = delete;
  /// Move ctor
  Observable(Observable&&) = delete;

  // ==========================================================================
  //                                Destructor
  // ==========================================================================
  /// Destructor
  virtual ~Observable() = default;

  // ==========================================================================
  //                                Operators
  // ==========================================================================
  /// Copy assignment
  Observable& operator=(const Observable& other) = delete;
  /// Move assignment
  Observable& operator=(const Observable&& other) = delete;

  // ==========================================================================
  //                              Public Methods
  // ==========================================================================
  /// Attach an observer o for a given event e
  inline void AddObserver(Observer* o, ObservableEvent e);
  /// Release an observer o for a given event e
  inline void DeleteObserver(Observer* o, ObservableEvent e);
  /// Notify all observers of event e providing optional argument
  inline void NotifyObservers(ObservableEvent e, void* arg = nullptr);

  // ==========================================================================
  //                                 Getters
  // ==========================================================================

  // ==========================================================================
  //                                 Setters
  // ==========================================================================

 protected :
  // ==========================================================================
  //                            Protected Methods
  // ==========================================================================

  // ==========================================================================
  //                               Attributes
  // ==========================================================================
  std::map<ObservableEvent, std::list<Observer*>> observers_;
};

/**
 *  \brief Attach an observer o for a given event e
 *
 * \param o Observer
 * \param e Event
 */
void Observable::AddObserver(Observer* o, ObservableEvent e) {
  observers_[e].emplace_back(o);
}


/**
 * \brief Release an observer o for a given event e
 *
 * \param o Observer
 * \param e Event
 */
void Observable::DeleteObserver(Observer* o, ObservableEvent e) {
  observers_[e].remove(o);
}


/**
 * \brief Notify all observers of event e providing optional argument
 *
 * \param e Event
 * \param arg Optional arguments provided to the observer
 */
void Observable::NotifyObservers(ObservableEvent e, void* arg) {
  for (auto& o : observers_[e]) {
    o->Update(*this, e, arg);
  }
}

}  // namespace hglib
#endif  // HGLIB_OBSERVABLE_H
