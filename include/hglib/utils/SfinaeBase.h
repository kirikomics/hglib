// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_SFINAEBASE_H
#define HGLIB_SFINAEBASE_H

namespace hglib {
/**
 * \brief Defines yes and no value for SFINAE.
 *
 * \details
 * Defines yes and no value for SFINAE - Substitution Failure Is Not An Error.\n
 * From <a href="http://en.cppreference.com/w/cpp/language/sfinae">
 * cppreference.com</a> : "This rule applies during overload resolution of
 * function templates: When substituting the deduced type for the template
 * parameter fails, the specialization is discarded from the overload set
 * instead of causing a compile error. This feature is used in template
 * metaprogramming."
 */
struct sfinae_base {
    /// yes
    typedef char (&yes)[1];
    /// no
    typedef char (&no)[2];
};

}  // namespace hglib
#endif  // HGLIB_SFINAEBASE_H
