// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HYPEREDGE_H
#define HGLIB_HYPEREDGE_H

// Base class for all hyperedges
#include "hglib/hyperedge/HyperedgeBase.h"

// Directed hyperedge
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/hyperedge/directed/NamedDirectedHyperedge.h"

// Undirected hyperedge
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedgeBase.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/NamedUndirectedHyperedge.h"

#endif  // HGLIB_HYPEREDGE_H
