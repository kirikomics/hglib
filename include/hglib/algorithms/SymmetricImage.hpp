// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_SYMMETRICIMAGE_HPP
#define HGLIB_SYMMETRICIMAGE_HPP


namespace hglib {


/**
 * \brief Build the symmetric image of a given directed hypergraph G = (V, E).
 *
 * \details
 * For all hyperarcs e in E, with e = ({tails}, {heads}) we create a hyperarc
 * e' = ({heads}, {tails}) in the symmetric image.
 *
 * \tparam DirectedHypergraph_t Directed hypergraph type.
 *
 * \param g Directed hypergraph.
 *
 * \return Unique_ptr to the symmetric image hypergraph.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph_t>
std::unique_ptr<DirectedHypergraph_t> symmetricImage(
        const DirectedHypergraph_t& g) {
  // Add vertices to symmetric image graph.
  // If a vertex with id x was removed previously from the given graph, then we
  // have to add and remove a vertex with id x in the symmetric image graph. We
  // have to fill the gaps in the sparse array. A gap at the end of the sparse
  // array can be ignored. The same holds for the hyperarcs.
  auto symmetricImage = std::make_unique<DirectedHypergraph_t>();
  VertexIdType expectedNextVertexId(0);
  for (const auto& vertex : g.vertices()) {
    auto args = g.argumentsToCreateVertex(vertex->id());
    if (vertex->id() != (expectedNextVertexId)) {
      // insert and remove (vertex->id() - expectedVertexId) times the current
      // vertex
      VertexIdType nbGapPositions = vertex->id() - expectedNextVertexId;
      do {
        const auto vertexCopy = symmetricImage->addVertex(
                args.name(), args.specificAttributes());
        symmetricImage->removeVertex(vertexCopy->id());
        --nbGapPositions;
      } while (nbGapPositions > 0);
    }
    symmetricImage->addVertex(args.name(), args.specificAttributes());
    expectedNextVertexId = vertex->id() + 1;
  }

  // Add symmetric image of hyperarcs
  HyperedgeIdType expectedNextHyperarcId(0);
  for (const auto& hyperarc : g.hyperarcs()) {
    auto args = g.hyperarcById(hyperarc->id())->argumentsToCreateHyperarc();
    if (hyperarc->id() != (expectedNextHyperarcId)) {
      HyperedgeIdType nbGapPositions = hyperarc->id() - expectedNextHyperarcId;
      do {
        const auto& hyperarcCopy = symmetricImage->addHyperarc(
                args.tailAndHeadIds(), args.specificAttributes());
        symmetricImage->removeHyperarc(hyperarcCopy->id());
        --nbGapPositions;
      } while (nbGapPositions > 0);
    }
    // swap tails and heads and insert the hyperarc to the symmetric image
    auto tailsAndHeads = args.tailAndHeadIds();
    auto tmp = tailsAndHeads.first;
    tailsAndHeads.first = tailsAndHeads.second;
    tailsAndHeads.second = tmp;
    args.setTailsAndHeadIds(tailsAndHeads);
    symmetricImage->addHyperarc(args.tailAndHeadIds(),
                                args.specificAttributes());
    expectedNextHyperarcId = hyperarc->id() + 1;
  }

  return symmetricImage;
}
}  // namespace hglib

#endif  // HGLIB_SYMMETRICIMAGE_HPP
