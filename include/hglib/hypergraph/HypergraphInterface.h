// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HYPERGRAPHINTERFACE_H
#define HGLIB_HYPERGRAPHINTERFACE_H

#include <fstream>
#include <functional>
#include <string>

#include "filtered_vector.h"

#include "hglib/utils/observation/Observable.h"
#include "hglib/utils/types.h"
#include "hglib/utils/HasInsertMemberFunction.h"
#include "hglib/vertex/ArgumentsToCreateVertex.h"

namespace hglib {

/**
 * \brief This abstract class defines the interface of a hypergraph.
 *
 * \details
 * This interface is currently implemented by Hypergraph and contains all
 * functions that are common to undirected and directed hypergraphs.
 *
 * @tparam VertexTemplate_t Vertex type.
 * @tparam Hyperedge_t Hyperedge type.
 * @tparam VertexProperty Vertex property type.
 * @tparam EdgeProperty Hyperedge property type.
 * @tparam HypergraphProperty Hypergraph property type.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperedge_t,
        typename VertexProperty,
        typename EdgeProperty,
        typename HypergraphProperty>
class HypergraphInterface : public Observable {
 public:
  /// Alias for the full vertex type
  using Vertex_t = VertexTemplate_t<Hyperedge_t>;
  /// Alias for VertexProperty
  using VertexProperty_t = VertexProperty;
  /// Alias for HypergraphProperty
  using HypergraphProperty_t = HypergraphProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = GraphElementContainer<Vertex_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename GraphElementContainer<
          Vertex_t*>::const_iterator;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = std::shared_ptr<GraphElementContainer<Vertex_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<VertexProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Constructor
  HypergraphInterface() : Observable() {}

 public:
  /// Destructor
  virtual ~HypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Size of the vertex container
  virtual size_t vertexContainerSize() const = 0;
  /// Number of vertices in the hypergraph
  virtual size_t nbVertices() const = 0;
  /// Search vertex list for a vertex with the provided name
  virtual const Vertex_t* vertexByName(
          const std::string& vertexName) const = 0;
  /// Search vertex list for a vertex with the provided id
  virtual const Vertex_t* vertexById(
          const VertexIdType & vertexId) const = 0;
  /// Const vertex iterator pointing to the begin of the vertices
  virtual vertex_iterator verticesBegin() const = 0;
  /// Const vertex iterator pointing to the end of the vertices
  virtual vertex_iterator verticesEnd() const = 0;
  /// Begin/End iterator pair of the vertices of the directed sub-hypergraph
  virtual std::pair<vertex_iterator,
          vertex_iterator> verticesBeginEnd() const = 0;

  /// Return a reference to the container of vertices
  virtual const VertexContainer& vertices() const = 0;

  /// Add a vertex
  virtual const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {}) = 0;
  /// Add a vertex
  virtual const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {}) = 0;

  /// Get arguments to create an existing vertex
  virtual ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the vertex with the provided id
  virtual const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const = 0;
  /// Get properties of the vertex with the provided id
  virtual VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph_impl property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hypergraph
  virtual const HypergraphProperty* getHypergraphProperties() const = 0;
  /// Get properties of the hypergraph
  virtual HypergraphProperty* getHypergraphProperties_() const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get ancestors
  virtual void getAncestors(std::vector<HypergraphInterface*>*) const = 0;
  /// Return shared_ptr to vertices container
  virtual VertexContainerPtr getRootVerticesContainerPtr() const = 0;
};


/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * \brief Retrieve graph's vertices into a container
 *
 * \details
 * Retrieve hypergraph's vertices and insert them into a container through the
 * provided output iterator.
 *
 * \param hypergraph graph from which to retrieve the vertices
 * \param output_iter
 * \parblock
 * OutputIterator to the container to fill \n
 * The underlying container's value_type should be `const Vertex_t*`
 * \endparblock
 * \param fn (optional) if provided, only those vertices for which
 * `fn(vertex->id())` evaluates to true will be inserted
 *
 * \par Examples
 * \parblock
 * Simple example
 * \code
 hglib::DirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
 for (auto arc : arcs) {
   g.addHyperarc(arc);
 }

 std::vector<const hglib::DirectedVertex<
    hglib::DirectedHyperedge>*> vertexContainer;
 hglib::vertices(g, std::back_inserter(vertexContainer));

 std::set<const hglib::DirectedVertex<hglib::DirectedHyperedge>*> s;
 hglib::vertices(g, std::inserter(s, s.begin()));
 * \endcode
 *
 * Retrieve vertices with a simple constraint
 * \code
 enum class Colour {
    red, blue, green, black
  };
  struct VertexProperty {
    string name = "Default";
    Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<hglib::DirectedVertex,
                            hglib::DirectedHyperedge,
                            VertexProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // Get green vertices
  std::vector<const hglib::DirectedVertex<
    hglib::DirectedHyperedge>*> greenVertices;
  hglib::vertices(g, std::back_inserter(greenVertices),
                   [&](hglib::VertexIdType vertexId) -> bool {
                     return g.getVertexProperties(vertexId)->colour ==
                            Colour::green;
                   });
 * \endcode
 * \endparblock
 *
 * \par Complexity
 * Linear most of the time\n
 * O(n * O(\a fn)) if \a fn is provided
 *
 * \relates HypergraphInterface
 */
template <typename HGraph, typename OutputIterator>
void vertices(const HGraph& hypergraph,
              OutputIterator output_iter,
              std::function<bool(hglib::VertexIdType)>&& fn = nullptr) {
  // Check that output_iter satisfies the OutputIterator requirements
  static_assert(
      std::is_same<
          typename std::iterator_traits<OutputIterator>::iterator_category,
          typename std::output_iterator_tag>::value,
      "provided iterator not an OutputIterator");

  // Insert vertices through output_iter
  for (const auto& vertex : hypergraph.vertices()) {
    // Insert if no fn provided or fn returns true
    if (not fn or fn(vertex->id())) {
      output_iter = vertex;
    }
  }
}

/**
 * \brief Export a (un-)directed hypergraph to a file
 *
 * Prints the given hypergraph to a file using the operator<<.
 *
 * \param hypergraph To be exported hypergraph.
 * \param filename Name of the file.
 *
 * \relates HypergraphInterface
 */
template <typename HGraph>
void exportHypergraph(const HGraph& hypergraph, const char* filename) {
  std::ofstream ofs(filename, std::ofstream::out);
  ofs << hypergraph;
  ofs.close();
}
}  // namespace hglib
#endif  // HGLIB_HYPERGRAPHINTERFACE_H
