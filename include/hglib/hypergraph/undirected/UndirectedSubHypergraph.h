// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDSUBHYPERGRAPH_H
#define HGLIB_UNDIRECTEDSUBHYPERGRAPH_H

#include <unordered_set>

#include "UndirectedHypergraphInterface.h"

#include "hglib/utils/observation/Observer.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {
/**
 * \brief This is a UndirectedSubHypergraph class template.
 *
 * \details
 * This class template allows to build views of an undirected hypergraph, that
 * is to consider a subset of vertices and hyperedges. A hierarchy of views can
 * be built, where each sub-hypergraph observes changes in its parent
 * (sub-) hypergraph. The removal of vertices and hyperedges in the parent
 * hypergraph yields in the removal of these vertices/hyperedges from all its
 * children views. Calling resetVertexIds() or resetHyperedgeIds() in the root
 * hypergraph results in updated vertex/hyperedge identifiers; these changes are
 * forwarded to all views.\n
 * An undirected sub-hypergraph can be built through a factory method:
 * \code
 * // Data
 * std::vector<std::string> vertices {"A", "B", "Foo", "Bar", "", "6"};
 * std::vector<std::pair<std::vector<std::string>, std::string>> hyperedges {
 *   {{"A", "Foo"}, "edge1"},
 *   {{"A", "B", "Bar", ""}, "edge2"},
 *   {{"6", ""}, "edge3"}
 * };
 *
 * // Build the root hypergraph
 * hglib::UndirectedHypergraph<> g;
 * // Add some vertices
 * for (auto vertexName : vertices) {
 *   g.addVertex(vertexName);
 * }
 * // Add hyperedges
 * for (auto hyperedge : hyperedges) {
 *   // Unnamed hyperedges (Do not use hyperedge.second to add a hyperedge)
 *   g.addHyperedge(hglib::NAME, hyperedge.first);
 * }
 *
 * // Build a subgraph of the root hypergraph g
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
 * auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
 *                                                      whitelistedHyperedges);
 *
 * // Build a sub-sub-hypergraph
 * whitelistedVertices = {1};
 * whitelistedHyperedges = {1};
 * auto subsubgraph =
 *     hglib::createUndirectedSubHypergraph(subgraph.get(),
 *                                          whitelistedVertices,
 *                                          whitelistedHyperedges);
 * \endcode
 *
 * \tparam VertexTemplate_t Vertex type. Defaults to UndirectedVertex.
 * \tparam Hyperedge_t Hyperedge type. Defaults to UndirectedHyperedge.
 * \tparam VertexProperty Vertex property type. Defaults to emptyProperty.
 * \tparam HyperedgeProperty Hyperedge property type. Defaults to emptyProperty.
 * \tparam HypergraphProperty Undirected hypergraph property type. Defaults to
 * emptyProperty.
 */
template<template<typename> typename VertexTemplate_t = UndirectedVertex,
        typename Hyperedge_t = UndirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperedgeProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class UndirectedSubHypergraph :
    public UndirectedHypergraphInterface<VertexTemplate_t,
                                         Hyperedge_t,
                                         VertexProperty,
                                         HyperedgeProperty,
                                         HypergraphProperty>,
    public Observer {
  template <template<typename> typename DirectedVertex_t,
            typename Arc_t,
            typename VertexProp,
            typename EdgeProp,
            typename HypergraphProp>
  friend class UndirectedHypergraph;

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Hyperedge_type;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeProperty_t;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainer;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::hyperedge_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  using VertexNames = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexNames;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  using VertexIds = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgePropertyContainerPtr;


  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// UndirectedSubHypergraph constructor
  UndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>* parent,
                          const std::unordered_set<VertexIdType>&
                          whitelistedVertexIds,
                          const std::unordered_set<HyperedgeIdType>&
                          whitelistedHyperedgeIds);

  /// UndirectedSubHypergraph copy constructor
  UndirectedSubHypergraph(const UndirectedSubHypergraph& other);
  /// UndirectedSubHypergraph destructor
  virtual ~UndirectedSubHypergraph();
  /// Copy assignment operator
  UndirectedSubHypergraph& operator=(const UndirectedSubHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  bool allowMultiHyperedges() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Hyperedge access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Number of hyperedges in the sub-hypergraph
  size_t nbHyperedges() const override;
  /// Size of the hyperedge container
  size_t hyperedgeContainerSize() const override;

  /// Add a hyperedge
  const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /// Add a hyperedge
  const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /// Add a hyperedge
  const Hyperedge_t* addHyperedge(const HyperedgeIdType& hyperedgeId);

  /// Remove a hyperedge with the given id from the hypergraph
  void removeHyperedge(const HyperedgeIdType& hyperedgeId) override;

  /// Search hyperedge list for an hyperedge with the provided id
  const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const override;

  /// Const hyperedge iterator pointing to the begin of the hyperedges
  hyperedge_iterator hyperedgesBegin() const override;
  /// Const hyperedge iterator pointing to the end of the hyperedges
  hyperedge_iterator hyperedgesEnd() const override;
  /// Begin/End iterator pair of the hyperedges of the sub-hypergraph
  std::pair<hyperedge_iterator,
          hyperedge_iterator> hyperedgesBeginEnd() const override;
  /// Return a reference to the container of hyperedges
  const HyperedgeContainer& hyperedges() const override;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with the given id
  size_t nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Check if the hyperedge with the given id has vertices
  bool hasVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to the begin of the implied vertices
  vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to the end of the implied vertices
  vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the implied vertices
  std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get shared_ptr of the container of implied vertices
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> impliedVertices(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Size of the vertex container
  size_t vertexContainerSize() const override;
  /// Number of vertices in the sub-hypergraph
  size_t nbVertices() const override;
  /// Search vertex list for a vertex with the provided name
  const Vertex_t* vertexByName(const std::string& vertexName) const override;
  /// Search vertex list for a vertex with the provided id
  const Vertex_t* vertexById(const VertexIdType & vertexId) const override;
  /// Const vertex iterator pointing to the begin of the vertices
  vertex_iterator verticesBegin() const override;
  /// Const vertex iterator pointing to the end of the vertices
  vertex_iterator verticesEnd() const override;
  /// Begin/End iterator pair of the vertices of the sub-hypergraph
  std::pair<vertex_iterator, vertex_iterator> verticesBeginEnd() const override;

  /// Return a reference to the container of vertices
  const VertexContainer& vertices() const override;

  /// Add a vertex
  const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex
  const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex
  const Vertex_t* addVertex(const VertexIdType& vertexId);

  /// Get arguments to create an existing vertex
  ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const override;

  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with the given Id
  size_t nbHyperedges(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given Id is part of a hyperedge
  bool isContainedInAnyHyperedge(const VertexIdType& vertexId) const override;
  /// Iterator pointing to the begin of the hyperedges that contain the vertex
  hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const override;
  /// Iterator pointing to the end of the hyperedges that contain the vertex
  hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const override;
  /// Pair of iterators pointing to the begin/end of the vertex's hyperedges
  std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of hyperedges that contain the vertex
  std::shared_ptr<const GraphElementContainer<Hyperedge_t*>> hyperedges(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with the given name from the sub-hypergraph
  void removeVertex(const std::string& vertexName,
                    bool removeImpliedHyperedges = true) override;
  /// Remove a vertex with the given Id from the sub-hypergraph
  void removeVertex(const VertexIdType& vertexId,
                    bool removeImpliedHyperedges = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if the given vertex is part of the given hyperedge
  bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperedgeProperty access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the hyperedge with the provided id
  const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get properties of the hyperedge with the provided id
  HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the vertex with the provided id
  const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const override;
  /// Get properties of the vertex with the provided id
  VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the sub-hypergraph
  const HypergraphProperty* getHypergraphProperties() const override;
  /// Get properties of the sub-hypergraph
  HypergraphProperty* getHypergraphProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get ancestors
  void getAncestors(std::vector<HypergraphInterface<VertexTemplate_t,
      Hyperedge_t, VertexProperty, HyperedgeProperty,
      HypergraphProperty>*>*) const override;
  /// Get root hypergraph
  const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty,
          HypergraphProperty>* getRootHypergraph() const override;

 protected:
  /// Observer update method
  void Update(const Observable& o, ObservableEvent e, void* arg) override;
  /// Remove the given vertex from the filtered out vertices in ancestors
  void removeFilteredOutVertexInAncestors(const VertexTemplate_t<Hyperedge_t>*);
  /// Remove the given hyperedge from the filtered out hyperedges in ancestors
  void removeFilteredOutHyperedgeInAncestors(const Hyperedge_t*);
  /// Add vertex property in ancestors
  void addVertexPropertyInAncestors(const VertexIdType& vertexId);
  /// Add hyperedge property in ancestors
  void addHyperedgePropertyInAncestors(const HyperedgeIdType& hyperedgeId);

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Set pointer to parent hypergraph to nullptr
  void setParentOfChildsToNull();
  /// Throw an exception if the pointer to the parent is a nullptr
  void throwExceptionOnNullptrParent() const;
  /// Update the vertex container upon a call to resetVertexIds()
  void updateVertexContainerUponResetVertexIds();
  /// Update the hyperedge container upon a call to resetHyperedgeIds()
  void updateHyperedgeContainerUponResetHyperedgeIds();
  /// Return shared_ptr to vertices container of the root hypergraph
  VertexContainerPtr getRootVerticesContainerPtr() const override;
  /// Return shared_ptr to hyperedge container of the root hypergraph
  HyperedgeContainerPtr getRootHyperedgeContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Parent hypergraph
  UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t, VertexProperty,
  HyperedgeProperty, HypergraphProperty>* parent_;
  /// Container of whitelisted vertices
  VertexContainerPtr whitelistedVerticesList_;
  /// Property per vertex
  VertexPropertyContainerPtr whitelistedVertexProperties_;
  /// Container of whitelisted hyperarcs
  HyperedgeContainerPtr whitelistedHyperedgesList_;
  /// Property per hyperedge
  HyperedgePropertyContainerPtr whitelistedHyperedgeProperties_;
  /// SubHypergraph property
  HypergraphProperty* subHypergraphProperty_;
};

/**
 * Factory method to create an undirected sub-hypergraph.
 *
 * @tparam UndirectedHypergraph_t
 * @param g
 * @param whitelistedVertexIds
 * @param whitelistedHyperarcIds
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::unique_ptr<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>>
createUndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>* parent,
                            const std::unordered_set<VertexIdType>&
                            whitelistedVertexIds,
                            const std::unordered_set<HyperedgeIdType>&
                            whitelistedHyperedgeIds) {
  return std::make_unique<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>>(parent, whitelistedVertexIds,
                               whitelistedHyperedgeIds);
}


/**
 * Factory method to copy a given undirected sub-hypergraph
 *
 * @tparam UndirectedHypergraph_t
 * @param g
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::unique_ptr<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>>
copyUndirectedSubHypergraph(
        const UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>& g) {
  return std::make_unique<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>>(g);
}
}  // namespace hglib
#include "UndirectedSubHypergraph.hpp"
#endif  // HGLIB_UNDIRECTEDSUBHYPERGRAPH_H
