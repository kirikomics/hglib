// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HYPERGRAPH_H
#define HGLIB_HYPERGRAPH_H

#include <memory>
#include <string>
#include <sys/types.h>

#include "filtered_vector.h"

#include "HypergraphInterface.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/ArgumentsToCreateVertex.h"

namespace hglib {

/**
 * \brief Hypergraph class template
 *
 * \details
 * This class template is a base class for directed and undirected hypergraphs
 * and offers functions that are common to both types of hypergraphs. One can
 * not create an instance of Hypergraph. DirectedHypergraph and
 * UndirectedHypergraph inherits publicely from Hypergraph.
 *
 * @tparam Vertex_t Vertex type.
 * @tparam Hyperedge_t Hyperedge type.
 * @tparam VertexProperty Vertex property type. Defaults to emptyProperty.
 * @tparam EdgeProperty Hyperedge property type. Defaults to emptyProperty.
 * @tparam GraphProperty Hypergraph property type. Defaults to emptyProperty.
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty = emptyProperty,
          typename EdgeProperty = emptyProperty,
          typename HypergraphProperty = emptyProperty>
class Hypergraph : virtual public HypergraphInterface<VertexTemplate_t,
        Hyperedge_t, VertexProperty, EdgeProperty, HypergraphProperty> {
  // Declare any instantiation of this template class as friend
  template <template <typename> class, typename, typename, typename, typename>
  friend class Hypergraph;

 public:
  /// Alias for the vertex type
  using Vertex_t = typename HypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, EdgeProperty, HypergraphProperty>::Vertex_t;
  /// Alias for VertexProperty
  using VertexProperty_t = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::VertexProperty_t;
  /// Alias for HypergraphProperty
  using HypergraphProperty_t = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::HypergraphProperty_t;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::VertexContainer;
  ///
  using vertex_iterator = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename HypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Hypergraph constructor
  explicit Hypergraph(bool allowMultiHyperedges = true);
  /// Hypergraph copy constructor
  explicit Hypergraph(const Hypergraph& rhs);
  /// Conversion constructor
  template <typename OtherVP, typename OtherEP, typename OtherGP>
  explicit Hypergraph(const Hypergraph<VertexTemplate_t,
                                       Hyperedge_t,
                                       OtherVP,
                                       OtherEP,
                                       OtherGP>& other);

  /// Hypergraph destructor
  virtual ~Hypergraph();

  /// Copy Assignment operator
  Hypergraph& operator=(const Hypergraph& source);

  /* **************************************************************************
  * **************************************************************************
  *                    Vertex access methods
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Size of the vertex container
  size_t vertexContainerSize() const override;
  /// Number of vertices in the hypergraph
  size_t nbVertices() const override;
  /// Search vertex list for a vertex with the provided name
  const Vertex_t* vertexByName(const std::string& vertexName) const override;
  /// Search vertex list for a vertex with the provided id
  const Vertex_t* vertexById(const VertexIdType & vertexId) const override;
  /// Const vertex iterator pointing to the begin of the vertices
  vertex_iterator verticesBegin() const override;
  /// Const vertex iterator pointing to the end of the vertices
  vertex_iterator verticesEnd() const override;
  /// Begin/End iterator pair of the vertices of the directed sub-hypergraph
  std::pair<vertex_iterator, vertex_iterator> verticesBeginEnd() const
    override;

  /// Return a reference to the container of vertices
  const VertexContainer& vertices() const override;

  /// Add a vertex
  const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex
  const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;

  /// Establish consecutive vertex ids
  void resetVertexIds();

  /// Get arguments to create an existing vertex
  ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the vertex with the provided id
  const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const override;
  /// Get properties of the vertex with the provided id
  VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph property access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the hypergraph
  const HypergraphProperty* getHypergraphProperties() const override;
  /// Get properties of the hypergraph
  HypergraphProperty* getHypergraphProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get ancestors
  void getAncestors(std::vector<HypergraphInterface<VertexTemplate_t,
      Hyperedge_t, VertexProperty, EdgeProperty,
      HypergraphProperty>*>*) const override;

  /* **************************************************************************
   * **************************************************************************
   *                         Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Search vertex list for a vertex with the provided name
  Vertex_t* vertexByName_(const std::string& vertexName) const;
  /// Remove nullptr entries from vertex properties container
  void removeNullEntriesFromVertexProperties();
  /// Get Ids of vertices for given vertex names
  std::vector<hglib::VertexIdType> getVertexIdsFromNames(
          const std::vector<std::string>& vertexNames) const;
  /// Set vertex and vertex property entry at given position to nullptr
  void setVertexAndVertexPropertyEntryToNullptr(const VertexIdType& idx);
  /// Return shared_ptr to vertices container
  VertexContainerPtr getRootVerticesContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Vertices of the hypergraph
  VertexContainerPtr vertices_;
  /// Property per vertex
  VertexPropertyContainerPtr vertexProperties_;
  /// Hypergraph property
  HypergraphProperty* hypergraphProperty_;
  /// Vertex max Id
  VertexIdType vertexMaxId_;
  /// allow multi-hyperedges
  bool allowMultiHyperedges_;
};
}  // namespace hglib
#include "Hypergraph.hpp"
#endif  // HGLIB_HYPERGRAPH_H
