// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_NAMEDDIRECTEDHYPEREDGE_H
#define HGLIB_NAMEDDIRECTEDHYPEREDGE_H

#include "DirectedHyperedgeBase.h"

#include <memory>
#include <tuple>
#include <utility>
#include <vector>
#include <string>

#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"
#include "ArgumentsToCreateDirectedHyperedge.h"

namespace hglib {

/**
 * \brief Named directed hyperedge class
 *
 * \details
 * This is a concrete class of a directed hyperedge.\n
 * All concrete classes derived from DirectedHyperedgeBase are final to
 * prohibit further derivation. The template parameter of the derived
 * DirectedHyperedgeBase is fixed to DirectedVertex\<Concrete_class\>, \em e.g.
 * DirectedVertex\<\em NamedDirectedHyperedge\>. The template parameters of a
 * directed hypergraph are then DirectedVertex\<NamedDirectedHyperedge\> and
 * NamedDirectedHyperedge for the vertex and directed hyperedge type.
 */
class NamedDirectedHyperedge final :
    public DirectedHyperedgeBase<DirectedVertex<NamedDirectedHyperedge>> {
  template <template <typename> typename DirectedVertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedHypergraphInterface;

  template <template <typename> typename Vertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedHypergraph;

  template <template <typename> typename Vertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedSubHypergraph;

  /// Alias for the vertex type
  using DirectedVertex_t = DirectedVertex<NamedDirectedHyperedge>;

  /* **************************************************************************
   * **************************************************************************
   *                               Nested class
   * **************************************************************************
   ***************************************************************************/
 public:
  /// SpecificAttributes nested class
  class SpecificAttributes;

  /* **************************************************************************
   * **************************************************************************
   *                              Constructors
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// NamedDirectedHyperedge constructor
  inline NamedDirectedHyperedge(
          int id,
          std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
          std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
          std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// NamedDirectedHyperedge copy constructor
  inline NamedDirectedHyperedge(const NamedDirectedHyperedge& other);

 public:
  /// Destructor
  virtual ~NamedDirectedHyperedge() = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Public member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get name of the directed hyperedge
  inline std::string name() const;
  /// Print the NamedDirectedHyperedge
  inline void print(std::ostream& out) const;
  /// Get arguments to create this NamedDirectedHyperedge
  inline ArgumentsToCreateDirectedHyperedge<NamedDirectedHyperedge>
  argumentsToCreateHyperarc() const;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Compare function
  inline bool compare(const std::vector<const DirectedVertex_t*>& tails,
                      const std::vector<const DirectedVertex_t*>& heads,
                      const SpecificAttributes& specificAttributes) const;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   ***************************************************************************/
 protected:
};

/**
 * \brief SpecificAttributes nested class
 *
 * \details
 * The nested class SpecificAttributes allows classes derived from
 * HyperedgeBase to specify additional attributes in a packed and
 * unified way.
 */
class NamedDirectedHyperedge::SpecificAttributes :
    public DirectedHyperedgeBase::SpecificAttributes {
 public:
  /// SpecificAttributes constructors
  SpecificAttributes() = delete;
  SpecificAttributes(const std::string& name) : name_(name) {}
  SpecificAttributes(const SpecificAttributes&) = default;
  SpecificAttributes(SpecificAttributes&&) = default;
  SpecificAttributes& operator=(const SpecificAttributes&) = default;
  SpecificAttributes& operator=(SpecificAttributes&&) = default;
  virtual ~SpecificAttributes() = default;

  /// Make a copy of the object
  std::unique_ptr<HyperedgeBase::SpecificAttributes> Clone() override {
    return std::make_unique<SpecificAttributes>(*this);
  };
  /// Name of the directed hyperedge
  std::string name_;
};

/**
 * \brief Get name of the directed hyperedge
 *
 * \return std::string Name of the directed hyperedge.
 */
std::string NamedDirectedHyperedge::name() const {
  return specificAttributes(*this).name_;
}

}  // namespace hglib

#include "NamedDirectedHyperedge.hpp"

#endif  // HGLIB_NAMEDDIRECTEDHYPEREDGE_H
