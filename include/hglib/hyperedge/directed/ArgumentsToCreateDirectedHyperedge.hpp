// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ArgumentsToCreateDirectedHyperedge.h"

namespace hglib {

/**
 * \brief ArgumentsToCreateDirectedHyperedge constructor
 *
 * \param tailAndHeadIds Identifiers of the tail and head vertices.
 * \param specificAttributes Attributes that are specific to the type of
 * directed hyperedge.
 */
template<typename Hyperarc_t>
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
ArgumentsToCreateDirectedHyperedge(
        const TailAndHeadIds& tailAndHeadIds,
        const typename Hyperarc_t::SpecificAttributes& specificAttributes) :
        tailAndHeadIds_(tailAndHeadIds),
        specificAttributes_(specificAttributes) {}


/**
 * \brief Get identifiers of tail and head vertices
 *
 * \return TailAndHeadIds Identifiers of tail and head vertices.
 */
template<typename Hyperarc_t>
const typename ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::TailAndHeadIds&
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
tailAndHeadIds() const {
  return tailAndHeadIds_;
}


/**
 * \brief Get specific attributes
 *
 * \return Hyperarc_t::SpecificAttributes Attributes that are specific for the
 * class template parameter Hyperarc_t.
 */
template<typename Hyperarc_t>
const typename Hyperarc_t::SpecificAttributes&
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * \brief Set identifiers of tail and head vertices
 *
 * \param tailHeadIds Identifiers of tail and head vertices (type
 * TailAndHeadIds).
 */
template<typename Hyperarc_t>
void ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
setTailsAndHeadIds(const typename ArgumentsToCreateDirectedHyperedge<
                Hyperarc_t>::TailAndHeadIds& tailHeadIds) {
  tailAndHeadIds_.first = tailHeadIds.first;
  tailAndHeadIds_.second = tailHeadIds.second;
}


/**
 * \brief Set attributes that are specific to the directed hyperedge type
 *
 * \param args Attributes that are specific to the directed hyperedge type.
 */
template<typename Hyperarc_t>
void ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
setSpecificArguments(const typename Hyperarc_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
