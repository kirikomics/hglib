// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDHYPEREDGE_H
#define HGLIB_UNDIRECTEDHYPEREDGE_H

#include "UndirectedHyperedgeBase.h"

#include <memory>

#include "ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {
/**
 * \brief Undirected hyperedge class
 *
 * \details
 * This is a concrete class of an undirected hyperedge.\n
 * All concrete classes derived from UndirectedHyperedgeBase are final to
 * prohibit further derivation. The template parameter of the derived
 * UndirectedHyperedgeBase is fixed to UndirectedVertex\<Concrete_class\>,
 * \em e.g. UndirectedVertex\<\em UndirectedHyperedge\>. The template parameters
 * of an undirected hypergraph are then UndirectedVertex\<UndirectedHyperedge\>
 * and UndirectedHyperedge for the vertex and undirected hyperedge type.
 */
class UndirectedHyperedge final :
        public UndirectedHyperedgeBase<UndirectedVertex<UndirectedHyperedge>>{
  template <template <typename> typename DirectedVertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedHypergraph;

  template <template <typename> typename DirectedVertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedHypergraphInterface;

  template <template <typename> typename Vertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

  /// Alias for the vertex type
  using Vertex_t = UndirectedVertex<UndirectedHyperedge>;

 public:
  /// SpecificAttributes nested class
  class SpecificAttributes;

  /* **************************************************************************
   * **************************************************************************
   *                              Constructors
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// UndirectedHyperedge constructor
  inline UndirectedHyperedge(
      int id,
      std::shared_ptr<GraphElementContainer<Vertex_t*>> vertices,
      std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// UndirectedHyperedge copy constructor
  inline UndirectedHyperedge(const UndirectedHyperedge& other);

 public:
  /// Destructor
  virtual ~UndirectedHyperedge() = default;
  /* **************************************************************************
   * **************************************************************************
   *                        Public member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get arguments to create this UndirectedHyperedge
  inline ArgumentsToCreateUndirectedHyperedge<UndirectedHyperedge>
  argumentsToCreateHyperedge() const;
  /// Print the undirected hyperedge
  inline void print(std::ostream& out) const;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Compare function
  inline bool compare(const std::vector<const Vertex_t*>& vertices,
               const SpecificAttributes& specificAttributes) const;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   ***************************************************************************/
 protected:
};

/**
 * \brief SpecificAttributes nested class
 *
 * \details
 * The nested class SpecificAttributes allows classes derived from
 * HyperedgeBase to specify additional attributes in a packed and
 * unified way.
 */
class UndirectedHyperedge::SpecificAttributes :
    public UndirectedHyperedgeBase::SpecificAttributes {
 public:
  /// Make a copy of the object
  std::unique_ptr<HyperedgeBase::SpecificAttributes> Clone() override {
    return std::make_unique<SpecificAttributes>();
  }
};

}  // namespace hglib

#include "hglib/hyperedge/undirected/UndirectedHyperedge.hpp"

#endif  // HGLIB_UNDIRECTEDHYPEREDGE_H
