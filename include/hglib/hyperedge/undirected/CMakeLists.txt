
# Append files in this directory to header files list
list(APPEND ${PROJECT_NAME}_HEADER_FILES
  ${CMAKE_CURRENT_LIST_DIR}/ArgumentsToCreateUndirectedHyperedge.h
  ${CMAKE_CURRENT_LIST_DIR}/ArgumentsToCreateUndirectedHyperedge.hpp
  ${CMAKE_CURRENT_LIST_DIR}/UndirectedHyperedge.h
  ${CMAKE_CURRENT_LIST_DIR}/UndirectedHyperedge.hpp
  ${CMAKE_CURRENT_LIST_DIR}/UndirectedHyperedgeBase.hpp
  ${CMAKE_CURRENT_LIST_DIR}/UndirectedHyperedgeBase.h
  ${CMAKE_CURRENT_LIST_DIR}/NamedUndirectedHyperedge.h
  ${CMAKE_CURRENT_LIST_DIR}/NamedUndirectedHyperedge.hpp)
