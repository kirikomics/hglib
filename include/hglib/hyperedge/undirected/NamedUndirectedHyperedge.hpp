/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <memory>

namespace hglib {
/**
 * \brief NamedUndirectedHyperedge constructor
 *
 * \param id Identifier of the DirectedHyperedge.
 * \param vertices Shared_ptr to the container of pointers to vertices.
 * \param specificAttributes Const reference to SpecificAttributes (contains
 * the name).
 */
NamedUndirectedHyperedge::
NamedUndirectedHyperedge(
    int id,
    std::shared_ptr<GraphElementContainer<Vertex_t*>> vertices,
    std::unique_ptr<HyperedgeBase::SpecificAttributes>&& specificAttributes) :
    UndirectedHyperedgeBase(id, vertices, std::move(specificAttributes)) {}


/**
 * \brief NamedUndirectedHyperedge copy constructor
 *
 * \param other To be copied NamedUndirectedHyperedge.
 */
NamedUndirectedHyperedge::
NamedUndirectedHyperedge(const NamedUndirectedHyperedge& other) :
    UndirectedHyperedgeBase(other) {}


/**
 * \brief Get arguments to create this NamedUndirectedHyperedge
 *
 * \details
 * This function returns an instance of ArgumentsToCreateUndirectedHyperedge
 * that provides all arguments (vertices Ids, specific attributes) to add this
 * named undirected hyperedge to a hypergraph.
 *
 * \par Example
 * \parblock
 * \code
 * // Hypergraphs
 * hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge> g,g2;
 * // Add vertices and undirected hyperedges to hypergraphs
 * // ...
 *
 * // Get arguments to create a named undirected hyperedge with Id hyperedgeId
 * auto args = g.argumentsToCreateHyperedge(hyperedgeId);
 * // Add undirected hyperedge to another hypergraph g2. It is assumed that the
 * // vertices are present in the hypergraph g2.
 * const auto& hyperpedge2 = g2.addHyperedge(args.verticesIds(),
 *                                           args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \return ArgumentsToCreateUndirectedHyperedge\<NamedUndirectedHyperedge\>
 * Arguments to add a named undirected hyperedge to a hypergraph via the
 * function addHyperedge().
 */
ArgumentsToCreateUndirectedHyperedge<NamedUndirectedHyperedge>
NamedUndirectedHyperedge::
argumentsToCreateHyperedge() const {
  std::vector<hglib::VertexIdType> vertices;
  for (const auto vertex : *(vertices_.get())) {
    vertices.push_back(vertex->id());
  }
  return ArgumentsToCreateUndirectedHyperedge<NamedUndirectedHyperedge>(
      vertices,
      specificAttributes(*this));
}

/**
 * \brief Get the name of the undirected hyperedge
 *
 * \return std::string Name of the directed hyperedge.
 */
std::string NamedUndirectedHyperedge::name() const {
  return specificAttributes(*this).name_;
}

/**
 * \brief Print the named undirected hyperedge
 *
 * \details
 * Prints 'Name_of_hyperedge: ' followed by the vertices of the
 * undirected hyperedge in a comma separated list. The
 * way a vertex is appended to the ostream depends on the implementation of the
 * print() method of the type of undirected vertex (template parameter
 * UndirectedVertex_t), \em e.g. the print() method in UndirectedVertex appends
 * the name of the vertex.
 *
 * \param out std::ostream
 */
void NamedUndirectedHyperedge::
print(std::ostream& out) const {
  out << name() << ": ";
  UndirectedHyperedgeBase<Vertex_t>::printVertices(out);
}

/**
 * \brief Compare function
 *
 * \details
 * Compare if the provided Ids of the vertices and the hyperedge specific
 * arguments are identical to the attributes of this NamedUndirectedHyperedge.
 *
 * \param vertices Pointers to vertices.
 * \param specificAttributes Specific attributes of a undirected hyperedge.
 *
 * \return True if this NamedUndirectedHyperedge has the same name, and vertex
 * Ids, false otherwise.
 *
 * \par Complexity
 * Linear in the number of vertices of this and the given pointers to
 * vertices.
 */
bool NamedUndirectedHyperedge::
compare(const std::vector<const Vertex_t*>& vertices,
        const SpecificAttributes& specificAttributes) const {
  return specificAttributes.name_.compare(name()) == 0 and
         UndirectedHyperedgeBase<Vertex_t>::compare(vertices);
}

}  // namespace hglib
