// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDHYPEREDGEBASE_H
#define HGLIB_UNDIRECTEDHYPEREDGEBASE_H

#include "hglib/hyperedge/HyperedgeBase.h"

#include <memory>
#include <vector>

#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {

template <typename Hyperedge_t>
class UndirectedVertex;

/**
 * \brief Base template class of an undirected hyperedge
 *
 * \details
 * This base template class of an undirected hyperedge contains attributes that
 * are common to all types of undirected hyperedges, that is an identifier, and
 * a container of implied vertices.\n
 * An undirected hyperedge relates any number of vertices.\n
 * The template parameter specifies the type of vertices in the undirected
 * hyperedge.\n
 * This class is an abstract class. To create an undirected hypergraph with some
 * type of undirected hyperedge, one needs to use a derived class of
 * UndirectedHyperedgeBase, \em e.g. UndirectedHyperedge or
 * NamedUndirectedHyperedge.
 * One may derive a class \em A from UndirectedHyperedgeBase to model a custom
 * undirected hyperedge. The class \em A must derive from
 * UndirectedHyperedgeBase<T>, where \em T is an undirected vertex type
 * (\em e.g. UndirectedVertex) which itself is  a class template. The template
 * parameter of the undirected vertex type is the type of undirected hyperedge,
 * \em e.g. \em A. Thus, the class \em A may derive from
 * UndirectedHyperedgeBase\<UndirectedVertex\<A\>\>. Any other undirected
 * vertex type class (must be derived from UndirectedVertex) can be used in this
 * derivation. This kind of curiously recurring template pattern (CRTP) is used
 * to achieve static polymorphism.\n\n
 * Editing an undirected hyperedge, \em e.g. adding/removing a vertex is
 * done via the undirected hypergraph.
 *
 *
 * \tparam UndirectedVertex_t Type of an undirected vertex class template.
 */
template <typename UndirectedVertex_t>
class UndirectedHyperedgeBase : public HyperedgeBase {
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraph;
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

 protected:
  /// UndirectedHyperedgeBase constructor
  UndirectedHyperedgeBase(
      HyperedgeIdType id,
      std::shared_ptr<GraphElementContainer<UndirectedVertex_t*>> vertices,
      std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// UndirectedHyperedgeBase copy constructor
  UndirectedHyperedgeBase(const UndirectedHyperedgeBase& other);

 public:
  /// Destructor
  virtual ~UndirectedHyperedgeBase() = default;

 public:
  /// Print the undirected hyperedge
  void print(std::ostream& out) const;

 protected:
  /// Remove a vertex from the undirected hyperedge
  void removeVertex(const UndirectedVertex_t& vertexToBeDeleted);
  /// Compare method
  bool compare(const std::vector<const UndirectedVertex_t*>& vertices) const;
  /// Prints vertices of the undirected hyperedge
  void printVertices(std::ostream& out) const;
  /// Add a vertex
  void addVertex(UndirectedVertex_t* vertexPtr);

 protected:
  /// Vertices of the undirected hyperedge
  std::shared_ptr<GraphElementContainer<UndirectedVertex_t*>> vertices_;
};

/**
 * \brief Overload of the extraction operator
 *
 * \tparam UndirectedVertex_t Undirected vertex type.
 *
 * \param out Reference to std::ostream.
 * \param hyperedge To be printed hyperedge.
 *
 * \return std::ostream
 *
 * \relates UndirectedHyperedgeBase
 */
template <typename UndirectedVertex_t>
std::ostream& operator<< (
        std::ostream& out,
        const UndirectedHyperedgeBase<UndirectedVertex_t>& hyperedge) {
  hyperedge.print(out);
  return out;
}

}  // namespace hglib

#include "UndirectedHyperedgeBase.hpp"
#endif  // HGLIB_UNDIRECTEDHYPEREDGEBASE_H
