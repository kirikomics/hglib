
# List header files
set(${PROJECT_NAME}_HEADER_FILES
  ${CMAKE_CURRENT_LIST_DIR}/hglib.h
  CACHE INTERNAL
  "${PROJECT_NAME}: Header Files")

include(${CMAKE_CURRENT_LIST_DIR}/hglib/CMakeLists.txt)
