/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include "FilteredVectorDecorator.h"

/// Default ctor
template<typename T>
FilteredVectorDecorator<T>::
FilteredVectorDecorator(std::shared_ptr<FilteredVector<T>> decorated,
                          std::shared_ptr<Filter<T>> filter) :
        FilteredVector<T>(filter),
        decorated_filtered_vector_(decorated) {
}


/// Move constructor
template<typename T>
FilteredVectorDecorator<T>::
FilteredVectorDecorator(FilteredVectorDecorator<T>&& other) :
        decorated_filtered_vector_(
                std::move(other.decorated_filtered_vector_)) {
  this->filter_ = std::move(other.filter_);
}



/// Move assignment
template<typename T>
FilteredVectorDecorator<T>& FilteredVectorDecorator<T>::
operator=(FilteredVectorDecorator&& rhs) {
  if (this != &rhs) {
    // release filter
    delete this->filter_;
    // get filter from rhs
    this->filter_ = rhs.filter_;
    // reset filter of rhs
    rhs.filter_ = nullptr;
    decorated_filtered_vector_ = std::move(rhs.decorated_filtered_vector_);
  }
  return *this;
}


/// Return iterator to beginning
template<typename T>
typename FilteredVectorDecorator<T>::iterator FilteredVectorDecorator<T>::
begin() noexcept {
  auto it = iterator(decorated_filtered_vector_->data(),
                     this->shared_from_this());
  // If current element is filtered out, call operator++
  if (decorated_filtered_vector_->size() > 0 && this->filter_out(*it)) {
    ++it;
  }
  return it;
}


/// Return iterator to beginning
template<typename T>
typename FilteredVectorDecorator<
        T>::const_iterator FilteredVectorDecorator<T>::
begin() const noexcept {
  auto it = const_iterator(decorated_filtered_vector_->data(),
                           this->shared_from_this());
  // If current element is filtered out, call operator++
  if (decorated_filtered_vector_->size() > 0 && this->filter_out(*it)) {
    ++it;
  }
  return it;
}


/// Return iterator to end
template<typename T>
typename FilteredVectorDecorator<T>::iterator FilteredVectorDecorator<T>::
end() noexcept {
  auto endPos = decorated_filtered_vector_->data() +
                decorated_filtered_vector_->size();
  return iterator(endPos, this->shared_from_this());
}


/// Return iterator to end
template<typename T>
typename FilteredVectorDecorator<
        T>::const_iterator FilteredVectorDecorator<T>::
end() const noexcept {
  auto endPos = decorated_filtered_vector_->data() +
                decorated_filtered_vector_->size();
  return const_iterator(endPos, this->shared_from_this());
}


/// Return const_iterator to beginning
template<typename T>
typename FilteredVectorDecorator<
        T>::const_iterator FilteredVectorDecorator<T>::
cbegin() const noexcept {
  return const_iterator(begin());
}


/// Return const_iterator to end
template<typename T>
typename FilteredVectorDecorator<
        T>::const_iterator FilteredVectorDecorator<T>::
cend() const noexcept {
  auto endPos = decorated_filtered_vector_->data() +
                decorated_filtered_vector_->size();
  return const_iterator(endPos, this->shared_from_this());
}


/// Return reverse iterator to reverse beginning
template<typename T>
typename FilteredVectorDecorator<
        T>::reverse_iterator FilteredVectorDecorator<T>::
rbegin() noexcept {
  return reverse_iterator(end());
}


/// Return reverse iterator to reverse end
template<typename T>
typename FilteredVectorDecorator<
        T>::reverse_iterator FilteredVectorDecorator<T>::
rend() noexcept {
  return reverse_iterator(begin());
}


/// Return const_reverse_iterator to reverse beginning
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reverse_iterator FilteredVectorDecorator<T>::
crbegin() noexcept {
  return const_reverse_iterator(cend());
}


/// Return const_reverse_iterator to reverse end
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reverse_iterator FilteredVectorDecorator<T>::
crend() noexcept {
  return const_reverse_iterator(cbegin());
}


/// Return size
template<typename T>
typename FilteredVectorDecorator<T>::size_type FilteredVectorDecorator<T>::
size() const noexcept {
  return decorated_filtered_vector_->size();
}


/// Return maximum size
template<typename T>
typename FilteredVectorDecorator<T>::size_type FilteredVectorDecorator<T>::
max_size() const noexcept {
  return decorated_filtered_vector_->max_size();
}

/// Change size
template<typename T>
void FilteredVectorDecorator<T>::
resize(size_type n) {
  try {
    decorated_filtered_vector_->resize(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Return size of allocated storage capacity
template<typename T>
typename FilteredVectorDecorator<T>::size_type FilteredVectorDecorator<T>::
capacity() const noexcept {
  return decorated_filtered_vector_->capacity();
}


/// Test whether the filtered vector is empty
template<typename T>
bool FilteredVectorDecorator<T>::
empty() const noexcept {
  return (begin() == end());
}


/// Requests that the capacity of the filtered vector is at least n
template<typename T>
void FilteredVectorDecorator<T>::
reserve(size_type n) {
  try {
    decorated_filtered_vector_->reserve(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Non-binding request to the filtered vector to reduce its capacity to fit
/// its size.
template<typename T>
void FilteredVectorDecorator<T>::
shrink_to_fit() {
  try {
    decorated_filtered_vector_->shrink_to_fit();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the element at position n
template<typename T>
typename FilteredVectorDecorator<T>::reference FilteredVectorDecorator<T>::
operator[] (size_type n) {
  return decorated_filtered_vector_->operator[](n);
}


/// Returns a const reference to the element at position n
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reference FilteredVectorDecorator<T>::
operator[] (size_type n) const {
  return decorated_filtered_vector_->operator[](n);
}


/// Returns a reference to the element at position n
template<typename T>
typename FilteredVectorDecorator<T>::reference FilteredVectorDecorator<T>::
at(size_type n) {
  try {
    return decorated_filtered_vector_->at(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a const reference to the element at position n
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reference FilteredVectorDecorator<T>::
at(size_type n) const {
  try {
    return decorated_filtered_vector_->at(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the first element
template<typename T>
typename FilteredVectorDecorator<T>::reference FilteredVectorDecorator<T>::
front() {
  try {
    return decorated_filtered_vector_->front();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a const reference to the first element
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reference FilteredVectorDecorator<T>::
front() const {
  try {
    return decorated_filtered_vector_->front();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the last element
template<typename T>
typename FilteredVectorDecorator<T>::reference FilteredVectorDecorator<T>::
back() {
  try {
    return decorated_filtered_vector_->back();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a const reference to the last element
template<typename T>
typename FilteredVectorDecorator<
        T>::const_reference FilteredVectorDecorator<T>::
back() const {
  try {
    return decorated_filtered_vector_->back();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Access data
template<typename T>
const typename FilteredVectorDecorator<
        T>::value_type* FilteredVectorDecorator<T>::
data() const noexcept {
  return decorated_filtered_vector_->data();
}


/// Access data
template<typename T>
typename FilteredVectorDecorator<
        T>::value_type* FilteredVectorDecorator<T>::
data() noexcept {
  return decorated_filtered_vector_->data();
}


// Modifiers
/// Assigns new contents to the vector
template<typename T>
void FilteredVectorDecorator<T>::
assign(iterator first, iterator last) {
  try {
    decorated_filtered_vector_->assign(first, last);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Assigns new contents to the vector
template<typename T>
void FilteredVectorDecorator<T>::
assign(size_type n, const value_type& val) {
  try {
    decorated_filtered_vector_->assign(n, val);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Assigns new contents to the vector
template<typename T>
void FilteredVectorDecorator<T>::
assign(std::initializer_list<value_type> il) {
  try {
    decorated_filtered_vector_->assign(il);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Add element at the end of filtered vector container
template<typename T>
void FilteredVectorDecorator<T>::
push_back(const value_type& val) {
  decorated_filtered_vector_->push_back(val);
}


/// Delete last element
template<typename T>
void FilteredVectorDecorator<T>::
pop_back() {
  try {
    decorated_filtered_vector_->pop_back();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Clear content
template<typename T>
void FilteredVectorDecorator<T>::
clear() noexcept {
  decorated_filtered_vector_->clear();
}
