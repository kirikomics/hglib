/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.10.17.
//

#ifndef FILTERED_VECTOR_CREATE_FILTERED_VECTOR_H
#define FILTERED_VECTOR_CREATE_FILTERED_VECTOR_H

#include <memory>

#include "FilteredVectorBase.h"
#include "FilteredVectorDecorator.h"
#include "Filter.h"

/**
 * Factory to create a filtered_vector_base
 *
 * @tparam T type to be stored in the container
 * @return unique_ptr of a filtered_vector<T> (abstract base class)
 */
template<typename T>
std::unique_ptr<FilteredVector<T>> create_filtered_vector() {
  return std::make_unique<FilteredVectorBase<T>>();
}


/**
 * Factory to create a filtered_vector_decorator
 *
 * A given filtered_vector will be decorated by applying a filter on it.
 *
 * @tparam T type to be stored in the container
 * @param decorated Pointer to filtered_vector which will be decorated
 * @param filter Pointer to filter
 * @return unique_ptr of a filtered_vector<T> (abstract base class)
 */
template<typename T>
std::unique_ptr<FilteredVector<T>> create_filtered_vector(
    std::shared_ptr<FilteredVector<T>> decorated,
    std::shared_ptr<Filter<T>> filter) {
  return std::make_unique<FilteredVectorDecorator<T>>(decorated,
                                                      filter);
}

#endif  // FILTERED_VECTOR_CREATE_FILTERED_VECTOR_H
