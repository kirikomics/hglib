
# List header files
set(${PROJECT_NAME}_HEADER_FILES
  ${CMAKE_CURRENT_LIST_DIR}/filtered_vector.h
  CACHE INTERNAL
  "${PROJECT_NAME} header files")

include(${CMAKE_CURRENT_LIST_DIR}/filtered_vector/CMakeLists.txt)
