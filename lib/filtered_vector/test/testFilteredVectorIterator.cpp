/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include "gtest/gtest.h"

#include <memory>


#include "../include/filtered_vector/FilteredVector.h"
#include "../include/filtered_vector/create_filtered_vector.h"
#include "../include/filtered_vector/Filter.h"
#include "../include/filtered_vector/NullptrFilter.h"
#include "../include/filtered_vector/WhitelistFilter.h"

using std::make_unique;

/**
 * Test fixture
 */
class TestFilteredVectorIterator : public testing::Test {
 protected:
  virtual void SetUp() {
    pointerVec = create_filtered_vector<int*>();
    aVec = create_filtered_vector<A*>();
  }

  virtual void TearDown() {
    for (auto item : *(pointerVec.get())) {
      delete item;
    }
    for (auto item : (*aVec.get())) {
      delete item;
    }
  }

  std::vector<int> intVector = {0, 1, 2};

  class A {
   public:
    A() {}
    A(int an_int, char a_char) : an_int(an_int), a_char(a_char) {}

    int id() {
      return an_int;
    }

    friend std::ostream& operator<<(std::ostream& os, const A& a) {
      os << "an_int: " << a.an_int << " a_char: " << a.a_char;
      return os;
    }

   public:
    int an_int = 1;
    char a_char = 'a';
  };

 protected:
  std::shared_ptr<FilteredVector<int*>> pointerVec;
  std::shared_ptr<FilteredVector<A*>> aVec;
};

TEST_F(TestFilteredVectorIterator, IncrementOperator) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
    pointerVec->push_back(nullptr);
  }
  ASSERT_EQ(pointerVec->size(), 2 * intVector.size());

  auto it = pointerVec->begin();
  auto ref = intVector.begin();
  ASSERT_EQ(*(*it), *ref);
  auto it2 = pointerVec->begin();
  ASSERT_EQ(it++, pointerVec->begin());
  ASSERT_NE(it, pointerVec->begin());
  ASSERT_EQ(it, pointerVec->begin() + 1);
  ASSERT_EQ(*it, nullptr);
  ++it;
  ASSERT_EQ(it, pointerVec->begin() + 2);
  ref++;
  ASSERT_EQ(*(*it), *ref);

  ASSERT_NE(++it2, pointerVec->begin());
  ASSERT_EQ(it2, pointerVec->begin() + 1);
  ASSERT_NE(it2, pointerVec->begin() + 2);
  it2++;
  ASSERT_EQ(it, it2);
}

TEST_F(TestFilteredVectorIterator, IncrementOperator2) {
  std::shared_ptr<FilteredVector<A*>> aVecNullptr_filter =
      create_filtered_vector<A*>(aVec, make_unique<NullptrFilter<A*>>());
  std::vector<A*> whitelist;
  for (auto i : intVector) {
    aVec->push_back(new A(2 * i, 'x'));
    // Add even items to whitelist
    if ((i % 2) == 0) {
      whitelist.push_back(aVec->back());
    } else {
      whitelist.push_back(nullptr);
    }
    aVec->push_back(nullptr);
    whitelist.push_back(nullptr);
  }

  std::shared_ptr<FilteredVector<A*>> aVecWhitelist_filter =
      create_filtered_vector<A*>(aVec,
                                 make_unique<WhitelistFilter<A*>>(whitelist));

  ASSERT_EQ(aVec->size(), 2 * intVector.size());
  ASSERT_EQ(aVecNullptr_filter->size(), 2 * intVector.size());
  ASSERT_EQ(aVecWhitelist_filter->size(), 2 * intVector.size());

  // test NullptrFilter and WhitelistFilter
  auto it = aVecNullptr_filter->begin();
  auto it2 = aVecWhitelist_filter->begin();
  for (auto i : intVector) {
    ASSERT_NE(*it, nullptr);
    ASSERT_NE(*it2, nullptr);
    ASSERT_EQ((*it)->id(), 2 * i);
    if ((i % 2) == 0) {
      ASSERT_EQ((*it2)->id(), 2 * i);
      ASSERT_EQ(*it, *it2);
      ++it2;
    }
    ++it;
  }
}

TEST_F(TestFilteredVectorIterator, DecrementOperator) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }

  auto it = pointerVec->end();
  auto it2 = pointerVec->end();
  // Check return value of post-decr is initial value
  ASSERT_EQ(it--, it2);
  // Check the effect of post-decr
  ASSERT_NE(it, pointerVec->end());
  ASSERT_EQ(it, pointerVec->end() - 1);

  // Check return value of pre-decr is not initial value
  ASSERT_NE(--it2, pointerVec->end());
  // Check the effect of pre-decr
  ASSERT_EQ(it2, pointerVec->end() - 1);
  ASSERT_EQ(it, it2);
}

TEST_F(TestFilteredVectorIterator, DecrementOperator2) {
  std::shared_ptr<FilteredVector<A*>> aVecNullptr_filter =
      create_filtered_vector<A*>(aVec, make_unique<NullptrFilter<A*>>());
  std::vector<A*> whitelist;
  for (auto i : intVector) {
    aVec->push_back(new A(2 * i, 'x'));
    // Add even items to whitelist
    if ((i % 2) == 0) {
      whitelist.push_back(aVec->back());
    } else {
      whitelist.push_back(nullptr);
    }
    aVec->push_back(nullptr);
    whitelist.push_back(nullptr);
  }
  // create FilteredVector with whitelist filter
  std::shared_ptr<FilteredVector<A*>> aVecWhitelist_filter =
      create_filtered_vector<A*>(aVec,
                                 make_unique<WhitelistFilter<A*>>(whitelist));

  // Check decrement operator of FilteredVector with NullptrFilter
  auto it = aVecNullptr_filter->end();
  auto it2 = aVecNullptr_filter->end();
  // Check return value of post-decr is initial value
  ASSERT_EQ(it--, it2);
  // Check the effect of post-decr
  ASSERT_NE(it, aVecNullptr_filter->end());
  ASSERT_EQ(it, aVecNullptr_filter->end() - 1);

  // Check return value of pre-decr is not initial value
  ASSERT_NE(--it2, aVecNullptr_filter->end());
  // Check the effect of pre-decr
  ASSERT_EQ(it2, aVecNullptr_filter->end() - 1);
  ASSERT_EQ(it, it2);

  // Check decrement operator of FilteredVector with whitelist filter
  it = aVecWhitelist_filter->end();
  it2 = aVecWhitelist_filter->end();
  // Check return value of post-decr is initial value
  ASSERT_EQ(it--, it2);
  // Check the effect of post-decr
  ASSERT_NE(it, aVecWhitelist_filter->end());
  ASSERT_EQ(it, aVecWhitelist_filter->end() - 1);

  // Check return value of pre-decr is not initial value
  ASSERT_NE(--it2, aVecWhitelist_filter->end());
  // Check the effect of pre-decr
  ASSERT_EQ(it2, aVecWhitelist_filter->end() - 1);
  ASSERT_EQ(it, it2);
}

TEST_F(TestFilteredVectorIterator, DecrementOperator_ignorenull) {
  pointerVec->push_back(nullptr);
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }
  pointerVec->push_back(nullptr);
  pointerVec->push_back(nullptr);
  pointerVec->push_back(new int(9));

  auto it = pointerVec->end() - 1;
  auto it2 = pointerVec->end() - 1;

  --it;
  ASSERT_EQ(*it, nullptr);
  // Check we've NOT gone over both null elts
  ASSERT_EQ(it, pointerVec->end() - 2);

  it = pointerVec->begin();  // Point to the first non-null elt
  ASSERT_THROW(it--, std::out_of_range);
}

TEST_F(TestFilteredVectorIterator, Comparison) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }
  auto it = pointerVec->begin();
  auto it2 = pointerVec->begin();
  ++it2;
  ASSERT_NE(it, it2);
  ASSERT_FALSE(it == it2);
  ASSERT_TRUE(it <= it2);
  ASSERT_FALSE(it >= it2);
  ASSERT_TRUE(it < it2);
  ASSERT_FALSE(it > it2);
  ASSERT_TRUE(it != it2);

  ++it;
  ASSERT_EQ(it, it2);
  ASSERT_TRUE(it == it2);
  ASSERT_TRUE(it <= it2);
  ASSERT_TRUE(it >= it2);
  ASSERT_FALSE(it < it2);
  ASSERT_FALSE(it > it2);
  ASSERT_FALSE(it != it2);

  ++it;
  ASSERT_NE(it, it2);
  ASSERT_FALSE(it == it2);
  ASSERT_FALSE(it <= it2);
  ASSERT_TRUE(it >= it2);
  ASSERT_FALSE(it < it2);
  ASSERT_TRUE(it > it2);
  ASSERT_TRUE(it != it2);

  auto it3 = pointerVec->end();
  ASSERT_TRUE(it < it3);
  ASSERT_TRUE(it <= it3);
  ASSERT_FALSE(it > it3);
  ASSERT_FALSE(it >= it3);
  ASSERT_TRUE(it != it3);
  ASSERT_FALSE(it == it3);

  ++it3;
  auto it4 = pointerVec->end();
  ASSERT_TRUE(it4 < it3);
  ASSERT_TRUE(it4 <= it3);
  ASSERT_FALSE(it4 > it3);
  ASSERT_FALSE(it4 >= it3);
  ASSERT_TRUE(it4 != it3);
  ASSERT_FALSE(it4 == it3);
}

TEST_F(TestFilteredVectorIterator, Test_ArithmeticOperator) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }
  // test it +/-/+=/-=
  auto it = pointerVec->begin();
  ASSERT_EQ(it, it + 0);
  ASSERT_EQ(it, it - 0);
  ASSERT_EQ(it + pointerVec->size(), pointerVec->end());
  ASSERT_EQ(pointerVec->end() - pointerVec->size(), it);
  auto it2 = it;
  it2 += 0;
  ASSERT_EQ(it2, it);
  it2 -= 0;
  ASSERT_EQ(it2, it);
  for (size_t i = 1; i <= pointerVec->size(); ++i) {
    auto it3 = it + i;
    ASSERT_EQ(++it2, it3);
  }
  it2 = pointerVec->begin();
  for (size_t i = 1; i <= pointerVec->size(); ++i) {
    auto it3 = it + i;
    it2 += 1;
    ASSERT_EQ(it2, it3);
  }

  it = pointerVec->end();
  it2 = it;
  for (size_t i = 1; i <= pointerVec->size(); ++i) {
    auto it3 = it - i;
    ASSERT_EQ(--it2, it3);
  }
  it2 = pointerVec->end();
  for (size_t i = 1; i <= pointerVec->size(); ++i) {
    auto it3 = it - i;
    it2 -= 1;
    ASSERT_EQ(it2, it3);
  }

  // test it - it2
  int diff1 = 0, diff2 = pointerVec->size();
  for (it = pointerVec->begin(); it != pointerVec->end(); ++it) {
    ASSERT_EQ(it - pointerVec->begin(), diff1++);
    ASSERT_EQ(pointerVec->end() - it, diff2--);
  }

  // test it = i + it2
  it = pointerVec->begin();
  it2 = pointerVec->begin();
  for (size_t i = 1; i <= pointerVec->size(); ++i) {
    it = i + pointerVec->begin();
    ++it2;
    ASSERT_EQ(it, it2);
  }
  ASSERT_EQ(it, pointerVec->end());
  ASSERT_EQ(it2, pointerVec->end());
}

TEST_F(TestFilteredVectorIterator, Test_Assignment) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }

  auto it = pointerVec->begin();
  auto it2 = pointerVec->begin();
  while (it != pointerVec->end()) {
    ++it2;
    it += 1;
    auto it3 = it;
    ASSERT_EQ(it, it2);
    ASSERT_EQ(it, it3);
    ++it3;
    ASSERT_NE(it, it3);
  }

  while (it != pointerVec->begin()) {
    --it2;
    it -= 1;
    ASSERT_EQ(it, it2);
  }
}

TEST_F(TestFilteredVectorIterator, Test_DereferenceOperator) {
  for (auto i : intVector) {
    pointerVec->push_back(new int(i));
  }

  int i = 0;
  auto itDeref = pointerVec->begin();
  for (auto it = pointerVec->begin(); it != pointerVec->end(); ++it) {
    ASSERT_EQ(*(*it), *(itDeref[i]));
    ++i;
  }
}


TEST_F(TestFilteredVectorIterator, Test_Whitelist_filter) {
  std::shared_ptr<FilteredVector<A*>> aVecNullptr_filter =
      create_filtered_vector<A*>(aVec, make_unique<NullptrFilter<A*>>());
  std::vector<A*> whitelist;
  for (auto i : intVector) {
    aVec->push_back(new A(2 * i, 'x'));
    // Add even items to whitelist
    if ((i % 2) == 0) {
      whitelist.push_back(aVec->back());
    } else {
      whitelist.push_back(nullptr);
    }
    aVec->push_back(nullptr);
    whitelist.push_back(nullptr);
  }

  std::shared_ptr<FilteredVector<A*>> aVecWhitelist_filter =
      create_filtered_vector<A*>(aVec,
                                 make_unique<WhitelistFilter<A*>>(whitelist));

  ASSERT_EQ(aVec->size(), 2 * intVector.size());
  ASSERT_EQ(aVecNullptr_filter->size(), 2 * intVector.size());
  ASSERT_EQ(aVecWhitelist_filter->size(), 2 * intVector.size());

  // test NullptrFilter and WhitelistFilter
  auto it = aVecNullptr_filter->begin();
  auto it2 = aVecWhitelist_filter->begin();
  for (auto i : intVector) {
    ASSERT_NE(*it, nullptr);
    ASSERT_NE(*it2, nullptr);
    ASSERT_EQ((*it)->id(), 2 * i);
    if ((i % 2) == 0) {
      ASSERT_EQ((*it2)->id(), 2 * i);
      ASSERT_EQ(*it, *it2);
      ++it2;
    }
    ++it;
  }

  // add odd items to WhitelistFilter
  for (auto i : intVector) {
    if ((i % 2) != 0) {
      aVecWhitelist_filter->removeFilteredOutItem(aVec->operator[](2 * i));
    }
  }
  // Check that size of WhitelistFilter container has not changed
  ASSERT_EQ(aVecWhitelist_filter->size(), 2 * intVector.size());
  // test NullptrFilter and WhitelistFilter (which are identical now)
  it = aVecNullptr_filter->begin();
  it2 = aVecWhitelist_filter->begin();
  for (auto i : intVector) {
    ASSERT_NE(*it, nullptr);
    ASSERT_NE(*it2, nullptr);
    ASSERT_EQ((*it)->id(), 2 * i);
    ASSERT_EQ((*it2)->id(), 2 * i);
    ASSERT_EQ(*it, *it2);
    ++it2;
    ++it;
  }


  // remove even items from whitelist
  for (auto i : intVector) {
    if ((i % 2) == 0) {
      aVecWhitelist_filter->addFilteredOutItem(aVec->operator[](2 * i));
    }
  }

  // Check that size of WhitelistFilter container has not changed
  ASSERT_EQ(aVecWhitelist_filter->size(), 2 * intVector.size());

  // test NullptrFilter and WhitelistFilter
  it = aVecNullptr_filter->begin();
  it2 = aVecWhitelist_filter->begin();
  for (auto i : intVector) {
    ASSERT_NE(*it, nullptr);
    if (it2 != aVecWhitelist_filter->end()) {
      ASSERT_NE(*it2, nullptr);
    }
    ASSERT_EQ((*it)->id(), 2 * i);
    if ((i % 2) != 0) {  // whitelist contains only odd items
      ASSERT_EQ((*it2)->id(), 2 * i);
      ASSERT_EQ(*it, *it2);
      ++it2;
    }
    ++it;
  }
}
